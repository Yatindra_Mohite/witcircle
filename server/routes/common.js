var express = require('express');
var app = express();
const router = express.Router();
var bodyParser = require('body-parser');

var jsonParser = bodyParser.json()
// Parsers for POST data
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
var db = require('./config');
var md5 = require('md5');


router.post('/test', jsonParser, function (req, res) {
  var result = {};
  $query = "delete FROM course WHERE id = '" + req.body.id + "'";
  db.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    result = {
      'status': 'not',
      'data': rows
    };
    res.status(200).json(result);
  });
});

////////////21-11-17/////


router.post('/state_list', jsonParser, function (req, res) {
  var result = data = {};
  var country_id = req.body.country_id;
  if (country_id == '') {
    result = {
      'statusText': 'failed',
      'message': 'Invalid request parameter',
      'status': 412
    };
    res.status(412).json(result);
    return;
  }
    $query = "SELECT RegionId,Region FROM region where CountryId ='" + country_id + "'";
    db.query($query, function (err, rows, fields)
    {
        if (err) {
            result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
            };
            res.status(500).json(result);
            return;
        }
        if (rows.length > 0) {
            data = {
            'state_list': rows
            };
            result = {
            'statusText': 'success',
            'message': 'State List',
            'status': 200,
            'data': data
            };
            res.status(200).json(result);
        } else {
            result = {
            'statusText': 'failed',
            'message': 'Record not found',
            'status': 200,
            };
            res.status(200).json(result);
        }
    });
});

router.post('/city_list', jsonParser, function (req, res) {
    var result = data = {};
    var region_id = req.body.region_id;
    if (region_id == '') {
      result = {
        'statusText': 'failed',
        'message': 'Invalid request parameter',
        'status': 412
      };
      res.status(412).json(result);
      return;
    }
      $query = "SELECT CityId,City FROM city where RegionID ='" + region_id + "'";
      db.query($query, function (err, rows, fields)
      {
          if (err) {
              result = {
                  'statusText': 'failed',
                  'message': 'Something went wrong! please try again later.',
                  'status': 500
              };
              res.status(500).json(result);
              return;
          }
          if (rows.length > 0) {
              data = {
              'city_list': rows
              };
              result = {
              'statusText': 'success',
              'message': 'City List',
              'status': 200,
              'data': data
              };
              res.status(200).json(result);
          } else {
              result = {
              'statusText': 'failed',
              'message': 'Record not found',
              'status': 200,
              };
              res.status(200).json(result);
          }
      });
});

router.post('/course_list', jsonParser, function (req, res) {
    var result = data = {};
    if(typeof req.headers.secret_key=='undefined')
    {
       result = {
         'statusText': 'false',
         'message': 'Unauthorized access.',
         'status': 401
       };
       res.status(401).json(result);
       return;
    }else
    {
        $query_ch = "SELECT id FROM user WHERE user_token ='" + req.headers.secret_key + "'";
        db.query($query_ch, function (err, query_res, fields) {
            if (err)
            {
            result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
            };
            res.status(500).json(result);
            return;
            }
            if(query_res.length>0)
            {   
                $query = "SELECT * FROM course WHERE status = 1 ORDER BY id DESC";
                db.query($query, function (err, rows, fields)
                {
                    if (err) {
                        result = {
                            'statusText': 'failed',
                            'message': 'Something went wrong! please try again later.',
                            'status': 500
                        };
                        res.status(500).json(result);
                        return;
                    }
                    if (rows.length > 0) {
                        data = {
                        'course_list': rows
                        };
                        result = {
                        'statusText': 'success',
                        'message': 'Course List',
                        'status': 200,
                        'data': data
                        };
                        res.status(200).json(result);
                    } else {
                        result = {
                        'statusText': 'failed',
                        'message': 'Record not found',
                        'status': 200,
                        };
                        res.status(200).json(result);
                    }
                });
            }else
            {
                result = {
                    'statusText': 'false',
                    'message': 'Invalid token',
                    'status': 401
                };
                res.status(401).json(result);
                return;
            }
        });
    }
});

router.post('/subject_list', jsonParser, function (req, res) {
    var result = data = {};
    if(typeof req.headers.secret_key=='undefined')
    {
       result = {
         'statusText': 'false',
         'message': 'Unauthorized access.',
         'status': 401
       };
       res.status(401).json(result);
       return;
    }else
    {
        if(typeof req.body.course_id=='undefined' || req.body.course_id=='')
        {
            result = {
                'statusText': 'failed',
                'message': 'Invalid request parameter',
                'status': 412
              };
              res.status(412).json(result);
              return;
        }
        $query_ch = "SELECT id FROM user WHERE user_token ='" + req.headers.secret_key + "'";
        db.query($query_ch, function (err, query_res, fields) {
            if (err)
            {
            result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
            };
            res.status(500).json(result);
            return;
            }
            if(query_res.length>0)
            {   
                $query = "SELECT * FROM course_subject WHERE status = 1 AND course_id = '"+req.body.course_id+"' ORDER BY id DESC";
                db.query($query, function (err, rows, fields)
                {
                    if (err) {
                        result = {
                            'statusText': 'failed',
                            'message': 'Something went wrong! please try again later.',
                            'status': 500
                        };
                        res.status(500).json(result);
                        return;
                    }
                    if (rows.length > 0) {
                        data = {
                        'course_list': rows
                        };
                        result = {
                        'statusText': 'success',
                        'message': 'Subject List',
                        'status': 200,
                        'data': data
                        };
                        res.status(200).json(result);
                    } else {
                        result = {
                        'statusText': 'failed',
                        'message': 'Record not found',
                        'status': 200,
                        };
                        res.status(200).json(result);
                    }
                });
            }else
            {
                result = {
                    'statusText': 'false',
                    'message': 'Invalid token',
                    'status': 401
                };
                res.status(401).json(result);
                return;
            }
        });
    }
});

router.post('/chapter_list', jsonParser, function (req, res) {
    var result = data = {};
    if(typeof req.headers.secret_key=='undefined')
    {
       result = {
         'statusText': 'false',
         'message': 'Unauthorized access.',
         'status': 401
       };
       res.status(401).json(result);
       return;
    }else
    {
        if(typeof req.body.subject_id=='undefined' || req.body.subject_id=='')
        {
            result = {
                'statusText': 'failed',
                'message': 'Invalid request parameter',
                'status': 412
              };
              res.status(412).json(result);
              return;
        }
        $query_ch = "SELECT id FROM user WHERE user_token ='" + req.headers.secret_key + "'";
        db.query($query_ch, function (err, query_res, fields) {
            if (err)
            {
            result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
            };
            res.status(500).json(result);
            return;
            }
            if(query_res.length>0)
            {   
                $query = "SELECT * FROM chapter WHERE status = 1 AND subject_id = '"+req.body.subject_id+"' ORDER BY id DESC";
                db.query($query, function (err, rows, fields)
                {
                    if (err) {
                        result = {
                            'statusText': 'failed',
                            'message': 'Something went wrong! please try again later.',
                            'status': 500
                        };
                        res.status(500).json(result);
                        return;
                    }
                    if (rows.length > 0) {
                        data = {
                        'chapter_list': rows
                        };
                        result = {
                        'statusText': 'success',
                        'message': 'Subject List',
                        'status': 200,
                        'data': data
                        };
                        res.status(200).json(result);
                    } else {
                        result = {
                        'statusText': 'failed',
                        'message': 'Record not found',
                        'status': 200,
                        };
                        res.status(200).json(result);
                    }
                });
            }else
            {
                result = {
                    'statusText': 'false',
                    'message': 'Invalid token',
                    'status': 401
                };
                res.status(401).json(result);
                return;
            }
        });
    }
});


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////      main functions     ////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function truncate_array(arr, l) {
  var new_arr = [];
  for (var k = 0; k < arr.length; k++) {
    if (arr[k].length > l)
      new_arr.push(arr[k].substring(0, l) + '...');
    else
      new_arr.push(arr[k]);
  }
  return new_arr;
}

function seoUrl(string) {

  //Lower case everything
  string = string.toLowerCase();
  //Make alphanumeric (removes all other characters)
  //string = string.replace(/[^a-z0-9+]+/gi, '');
  //Clean up multiple dashes or whitespaces
  string = string.replace(/[^\w ]+/g, '');
  //Convert whitespaces and underscore to dash
  string = string.replace(/ +/g, '-');
  var x = Math.floor((Math.random() * 10000) + 1);
  string = string + x
  return string;
}

function ucfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function random_otp() {
  return val = Math.floor(1000 + Math.random() * 9999);
}

function getRandomInteger() {
  min = Math.ceil(1);
  max = Math.floor(10000);
  return Math.floor(Math.random() * (max - min)) + min;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////       MOBILE APP      ////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

module.exports = router;
