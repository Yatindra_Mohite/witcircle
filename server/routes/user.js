var express = require('express');
var multer  = require('multer')
var app = express();
const router = express.Router();
var bodyParser = require('body-parser');
var upload = multer({ dest: './uploads' }).any();

var jsonParser = bodyParser.json()
// Parsers for POST data
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
var db = require('./config');
var md5 = require('md5');


router.post('/test', jsonParser, function (req, res) {
  var result = {};
  $query = "delete FROM course WHERE id = '" + req.body.id + "'";
  db.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    result = {
      'status': 'not',
      'data': rows
    };
    res.status(200).json(result);
  });
});

////////////20-11-17/////

router.post('/registration', jsonParser, function (req, res) {
  var result = {};
  var time = new Date().getTime();
  var mobile = req.body.mobile;
  //  var otp = random_otp();
  var otp = '1234';
  if (typeof req.body.mobile != 'undefined') {
    $query_ch = "SELECT mobile,user_status FROM user WHERE mobile ='" + mobile + "'";
    db.query($query_ch, function (err, query_res, fields) {
      if (err) {
        result = {
          'statusText': 'failed',
          'message': 'Something went wrong! please try again later.',
          'status': 500
        };
        res.status(500).json(result);
        return;
      }
      if (query_res[0]) {
        if (query_res[0]['user_status'] == 1) {
          result = {
            'statusText': 'failed',
            'message': 'Mobile number already registered.',
            'status': 200
          };
          res.status(200).json(result);
          return;
        } else {
          $query = "UPDATE user SET `otp_code`='" + otp + "' WHERE `mobile`='" + mobile + "'";
          db.query($query, function (err, rows, fields) {
            if (err) {
              result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
              };
              res.status(500).json(result);
              return;
            } else {
              result = {
                'statusText': 'success',
                'message': 'OTP send to your registered mobile number.',
                'status': 200
              };
              res.status(200).json(result);
              return;
            }
          });
        }
      } else {
        $query = "INSERT INTO user (mobile,otp_code,create_at) VALUES('" + mobile + "','" + otp + "',NOW())";
        db.query($query, function (err, rows, fields) {
          if (rows) {
            result = {
              'statusText': 'success',
              'message': 'OTP send to your registered mobile number.',
              'status': 200
            };
            res.status(200).json(result);
          } else {
            result = {
              'statusText': 'failed',
              'message': 'Something went wrong! please try again later.',
              'status': 500
            };
            res.status(500).json(result);
          }
        });
      }
    });
  } else {
    result = {
      'statusText': 'failed',
      'message': 'Invalid request parameter',
      'status': 412
    };
    res.status(412).json(result);
  }
});

router.post('/otp_verification', jsonParser, function (req, res) {
  var result = data = {};
  var mobile = req.body.mobile;
  var otp = req.body.otp;
  if (mobile == '' || otp == '' || otp.length != 4) {
    result = {
      'statusText': 'failed',
      'message': 'Invalid request parameter',
      'status': 412
    };
    res.status(412).json(result);
    return;
  }
  $query = "SELECT id,otp_code FROM user where mobile ='" + mobile + "'";
  db.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'statusText': 'failed',
        'message': 'Something went wrong! please try again later.',
        'status': 500,
        'rows': rows
      };
      res.status(500).json(result);
      return;
    }
    if (rows.length > 0) {
      var user_id = rows[0]['id'];
      if (rows[0]['otp_code'] == otp) {
        $query = "UPDATE user SET `otp_code`='' WHERE `mobile`='" + mobile + "'";
        db.query($query, function (err, rows, fields) {
          if (err) {
            result = {
              'statusText': 'failed',
              'message': 'Something went wrong! please try again later.',
              'status': 500,
            };
            res.status(500).json(result);
            return;
          }
        });
        data = {
          'user_id': user_id
        };
        result = {
          'statusText': 'success',
          'message': 'OTP has been verified successfully',
          'status': 200,
          'data': data
        };
        res.status(200).json(result);
      } else {
        result = {
          'statusText': 'failed',
          'message': 'OTP does not match',
          'status': 200,
        };
        res.status(200).json(result);
      }
    } else {
      result = {
        'statusText': 'failed',
        'message': 'Account does not exists.',
        'status': 200,
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.post('/create_pin', jsonParser, function (req, res) {
  var result = data = {};
  var user_id = req.body.user_id;
  var pin_number = req.body.pin_number;
  if (pin_number == '' || pin_number.length != 4) {
    result = {
      'statusText': 'failed',
      'message': 'Invalid request parameter',
      'status': 200
    };
    res.status(200).json(result);
    return;
  }
  $query = "SELECT id FROM user where id ='" + user_id + "'";
  db.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'statusText': 'failed',
        'message': 'Something went wrong! please try again later.',
        'status': 500,
      };
      res.status(500).json(result);
      return;
    }
    if (rows.length > 0) {
      var time = new Date().getTime();
      var user_auth_token = md5(time) + time;
      $query = "UPDATE user SET `password`= '" + md5(pin_number) + "',`user_token` = '" + user_auth_token + "',`user_status` = 1 WHERE `id`='" + user_id + "'";
      db.query($query, function (err, rowss, fields) {
        if (err) {
          result = {
            'statusText': 'failed',
            'message': 'Something went wrong! please try again later.',
            'status': 500,
          };
          res.status(500).json(result);
          return;
        }
      });
      data = {
        'name': rows[0]['name'],
        'email_id': rows[0]['email_id'],
        'mobile_code': rows[0]['mobile_code'],
        'mobile': rows[0]['mobile'],
        'dob': rows[0]['dob'],
        'gender': rows[0]['gender'],
        'address': rows[0]['address'],
        'user_type': rows[0]['user_type'],
        'token': user_auth_token,
        'device_token': rows[0]['device_token'],
        'device_id': rows[0]['device_id'],
        'device_type': rows[0]['device_type'],
        'follow_coaching':rows[0]['follow_coaching']
      };
      result = {
        'statusText': 'success',
        'message': 'Pin successfully created',
        'status': 200,
        'data': data
      };
      res.status(200).json(result);
    } else {
      result = {
        'statusText': 'failed',
        'message': 'Account does not exists.',
        'status': 200,
      };
      res.status(200).json(result);
    }
  });
});

router.post('/login', jsonParser, function (req, res) {

  var result = data = {};
  var device_token = req.body.device_token;
  var device_id = req.body.device_id;
  var device_type = req.body.device_type;
  var mobile = req.body.mobile;
  var pin_number = req.body.pin_number;

  $query = "SELECT * FROM user WHERE mobile ='" + mobile + "' AND password ='" + md5(pin_number) + "' ";
  db.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'statusText': 'failed',
        'message': 'Something went wrong! please try again later.',
        'status': 500
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows[0]) {
      var user_id = rows[0]['id'];
      var time = new Date().getTime();
      var user_auth_token = md5(time) + time;
      $query2 = "UPDATE user SET device_token = '" + device_token + "',device_id = '" + device_id + "',device_type = '" + device_type + "',user_token = '" + user_auth_token + "' WHERE id='" + user_id + "'";

      db.query($query2, function (err1, rows1, fields) {
        if (err1) {
          result = {
            'statusText': 'failed',
            'message': 'Something went wrong! please try again later.',
            'status': 500
          };
          res.status(500).json(result);
          //console.log("An error ocurred performing the query.");
          return;
        }
        //console.log
        if (rows1.affectedRows > 0) {
          data = {
            'name': rows[0]['name'],
            'email_id': rows[0]['email_id'],
            'mobile_code': rows[0]['mobile_code'],
            'mobile': rows[0]['mobile'],
            'dob': rows[0]['dob'],
            'gender': rows[0]['gender'],
            'address': rows[0]['address'],
            'user_type': rows[0]['user_type'],
            'token': user_auth_token,
            'device_token': device_token,
            'device_id': device_id,
            'device_type': device_type,
            'follow_coaching':rows[0]['follow_coaching']
          };
          result = {
            'statusText': 'success',
            'message': 'Successfully login',
            'status': 200,
            'data': data
          };
          res.status(200).json(result);
        } else {
          result = {
            'statusText': 'failed',
            'message': 'Something went wrong! please try again later.',
            'status': 200
          };
          res.status(200).json(result);
        }
      });
    } else {
      result = {
        'statusText': 'failed',
        'message': 'Invalid login credentials',
        'status': 200,
      };
      res.status(200).json(result);
    }
  });

});

router.post('/coaching_registration', jsonParser, function (req, res) {
 if(typeof req.headers.secret_key=='undefined')
 {
    result = {
      'statusText': 'false',
      'message': 'Unauthorized access.',
      'status': 401
    };
    res.status(401).json(result);
    return;
 }else
 {
    if(req.body.coaching_name=='')
    {
        result = {
          'statusText': 'failed',
          'message': 'Invalid request parameter.',
          'status': 412
        };
        res.status(412).json(result);
        return;
    }else
    {
        $query_ch = "SELECT id FROM user WHERE user_token ='" + req.headers.secret_key + "'";
        db.query($query_ch, function (err, query_res, fields) {
        if (err)
        {
          result = {
            'statusText': 'failed',
            'message': 'Something went wrong! please try again later.',
            'status': 500
          };
          res.status(500).json(result);
          return;
        }
        if(query_res.length>0)
        { 
          $query_ch = "SELECT id FROM coaching WHERE user_id ='" + query_res[0]['id'] + "'";
          db.query($query_ch, function (err, queryres, fields) {
            if (err) {
              result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
              };
              res.status(500).json(result);
              return;
            }
            if(queryres[0])
            {
              result = {
                'statusText': 'failed',
                'message': 'Coaching already registered.',
                'status': 200
              };
              res.status(200).json(result);
            }else
            {
                $query = "INSERT INTO coaching (user_id,coaching_name,coaching_head,email_id,country,state,city,address,mobile,create_at) VALUES('" + query_res[0]['id'] + "','" + req.body.coaching_name + "', '" + req.body.coaching_head + "', '" + req.body.email_id + "', '" + req.body.country + "', '" + req.body.state + "', '" + req.body.city + "', '" + req.body.address + "', '" + req.body.mobile + "', NOW())";
                db.query($query, function (err, rows, fields) {
                if(err)
                {
                  result = {
                    'statusText': 'failed',
                    'message': 'Something went wrong! please try again later.',
                    'status': 500
                  };
                  res.status(500).json(result);
                }else
                { 
                  result = {
                    'statusText': 'success',
                    'message': 'Coaching successfully registered.',
                    'status': 200
                  };
                  res.status(200).json(result);
                }
              });
            }
          });
        }else{
          result = {
            'statusText': 'false',
            'message': 'Invalid token',
            'status': 401
          };
          res.status(401).json(result);
          return;
        }
      });
    }
  }  
});

router.post('/coaching_list', jsonParser, function (req, res) {
  var result = data = {};
  var follow_status = 0;
  var arrr = [];
  if(typeof req.headers.secret_key=='undefined')
  {
     result = {
       'statusText': 'false',
       'message': 'Unauthorized access.',
       'status': 401
     };
     res.status(401).json(result);
     return;
  }else
  {
      $query_ch = "SELECT id FROM user WHERE user_token ='" + req.headers.secret_key + "'";
      db.query($query_ch, function (err, query_res, fields) {
          if (err)
          {
          result = {
              'statusText': 'failed',
              'message': 'Something went wrong! please try again later.',
              'status': 500
          };
          res.status(500).json(result);
          return;
          }
          if(query_res.length>0)
          {   
              $query = "SELECT c.id,c.coaching_name,c.address,country.Country,region.Region,city.City,user.user_id FROM coaching as c INNER JOIN country ON c.country=country.CountryId INNER JOIN region ON c.state=region.RegionId INNER JOIN city ON c.city=city.CityId INNER JOIN user ON c.user_id = user.user_id WHERE c.status = 1 AND c.user_id != '"+query_res[0]['id']+"' AND user.user_status=1 AND user.admin_status=1 ORDER BY c.id DESC";
              db.query($query, function (err, rows, fields)
              {
                  if (err) {
                      result = {
                          'statusText': 'failed',
                          'message': 'Something went wrong! please try again later.',
                          'status': 500
                      };
                      res.status(500).json(result);
                      return;
                  }
                  if (rows.length > 0) {
                    for (var i in rows) {
                      // $query = "SELECT id FROM follow_coaching WHERE FIND_IN_SET(coaching_id,'"+ rows[i].id +"') AND user_id = '"+query_res[0]['id']+"'";
                      // db.query($query, function (err, follow_rows, fields)
                      // {
                      //   if (err) {
                      //     result = {
                      //         'statusText': 'failed',
                      //         'message': 'Something went wrong! please try again lateraa.',
                      //         'status': 500
                      //     };
                      //     res.status(500).json(result);
                      //     return;
                      //     }
                      //     if(follow_rows.length>0)
                      //     {
                      //       follow_status = 1;
                      //     }else
                      //     {
                      //       follow_status = 0;
                      //     }
                      // });
                      arrr.push({ 
                        //'follow_status':follow_status,
                        'coaching_name':rows[i].coaching_name,
                        'address':rows[i].address,
                        'country':rows[i].Country,
                        'region':rows[i].Region,
                        'city':rows[i].City,
                      });
                    }
                      data = {
                      'coaching_list': arrr
                      };
                      result = {
                      'statusText': 'success',
                      'message': 'Coaching List',
                      'status': 200,
                      'data': data
                      };
                      res.status(200).json(result);
                  } else {
                      result = {
                      'statusText': 'failed',
                      'message': 'Record not found',
                      'status': 200,
                      };
                      res.status(200).json(result);
                  }
              });
          }else
          {
              result = {
                  'statusText': 'false',
                  'message': 'Invalid token',
                  'status': 401
              };
              res.status(401).json(result);
              return;
          }
      });
  }
});

router.post('/follow_unfollow', jsonParser, function (req, res) {
  if(typeof req.headers.secret_key=='undefined')
  {
     result = {
       'statusText': 'false',
       'message': 'Unauthorized access.',
       'status': 401
     };
     res.status(401).json(result);
     return;
  }else
  {
     if(typeof req.body.coaching_id=='undefined')
     {
         result = {
           'statusText': 'failed',
           'message': 'Invalid request parameter.',
           'status': 412
         };
         res.status(412).json(result);
         return;
     }else
     {  
       var coaching_id = req.body.coaching_id;
        $query_ch = "SELECT id,follow_coaching FROM user WHERE user_token ='" + req.headers.secret_key + "'";
         db.query($query_ch, function (err, query_res, fields) {
         if (err)
         {
           result = {
             'statusText': 'failed',
             'message': 'Something went wrong! please try again later.',
             'status': 500
           };
           res.status(500).json(result);
           return;
         }
         if(query_res.length>0)
         { 
            $query = "UPDATE user SET follow_coaching = '"+coaching_id+"' WHERE id ='" + query_res[0]['id'] + "'";
            db.query($query, function (err, rows, fields)
            {
                if(err)
                {
                  result = {
                    'statusText': 'failed',
                    'message': 'Something went wrong! please try again later.',
                    'status': 500,
                  };
                  res.status(500).json(result);
                  return;
                }
                if(rows.affectedRows>0)
                {
                    result = {
                      'statusText': 'success',
                      'message': 'Follow list successfully updated.',
                      'status': 200
                    };
                    res.status(200).json(result);
                }else
                {
                    result = {
                      'statusText': 'failed',
                      'message': 'Something went wrong! please try again later.',
                      'status': 500
                    };
                    res.status(500).json(result);
                }
            });
         }else{
           result = {
             'statusText': 'false',
             'message': 'Invalid token',
             'status': 401
           };
           res.status(401).json(result);
           return;
         }
       });
     }
   }  
});

router.post('/forgot_password', jsonParser, function (req, res) {
  var result = data = {};
  var mobile = req.body.mobile;
  //  var otp = random_otp();
  var otp = '5678';
  if (typeof req.body.mobile != 'undefined' && req.body.mobile != '') {
    $query_ch = "SELECT id,user_status,admin_status FROM user WHERE mobile ='" + mobile + "'";
    db.query($query_ch, function (err, query_res, fields) {
      if (err) {
        result = {
          'statusText': 'failed',
          'message': 'Something went wrong! please try again later.',
          'status': 500
        };
        res.status(500).json(result);
        return;
      }
      if (query_res[0])
      {
        if (query_res[0]['user_status'] != 1) {
          result = {
            'statusText': 'failed',
            'message': 'Your account is not verified! please click on resend OTP button for new OTP code.',
            'status': 200
          };
          res.status(200).json(result);
          return;
        }else if (query_res[0]['admin_status'] != 1)
        {
          result = {
            'statusText': 'failed',
            'message': 'Currently your number is inactive from admin.',
            'status': 200
          };
          res.status(200).json(result);
          return;
        }
        else
        {
          $query = "UPDATE user SET `otp_code`='" + otp + "' WHERE `id`='" + query_res[0]['id'] + "'";
          db.query($query, function (err, rows, fields) {
            if (err) {
              result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
              };
              res.status(500).json(result);
              return;
            } 
            if(rows.affectedRows>0){
              data = { 'user_id': query_res[0]['id'] };
              result = {
                'statusText': 'success',
                'message': 'OTP send to your registered mobile number.',
                'status': 200,
                'data':data
              };
              res.status(200).json(result);
              return;
            }else{
              result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
              };
              res.status(500).json(result);
            }
          });
        }
      } else 
      {
        result = {
          'statusText': 'failed',
          'message': 'Account does not exists.',
          'status': 200
        };
        res.status(200).json(result);
      }
    });
  } else {
    result = {
      'statusText': 'failed',
      'message': 'Invalid request parameter',
      'status': 412
    };
    res.status(412).json(result);
  }
});

router.post('/change_pin_with_otp', jsonParser, function (req, res) {
  var result = data = {};
  var user_id = req.body.user_id;
  var pin_number = req.body.pin_number;
  var otp = req.body.otp;

  if (pin_number == '' || pin_number.length != 4 || otp.length != 4 || user_id == '') {
    result = {
      'statusText': 'failed',
      'message': 'Invalid request parameter',
      'status': 412
    };
    res.status(412).json(result);
    return;
  }
  $query = "SELECT otp_code FROM user where id ='" + user_id + "'";
  db.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'statusText': 'failed',
        'message': 'Something went wrong! please try again later.',
        'status': 500,
      };
      res.status(500).json(result);
      return;
    }
    if (rows.length > 0) {
      if(rows[0]['otp_code']==otp)
      {
          $query = "UPDATE user SET `password`= '" + md5(pin_number) + "',`otp_code`= '' WHERE `id`='" + user_id + "'";
          db.query($query, function (err, rowss, fields) {
            if (err) {
              result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500,
              };
              res.status(500).json(result);
              return;
            }
          });
          result = {
            'statusText': 'success',
            'message': 'Pin successfully changed',
            'status': 200,
          };
          res.status(200).json(result);
      }else
      {
        result = {
          'statusText': 'failed',
          'message': 'OTP does not match.',
          'status': 200,
        };
        res.status(200).json(result);
      }
    } else {
      result = {
        'statusText': 'failed',
        'message': 'Account does not exists.',
        'status': 200,
      };
      res.status(200).json(result);
    }
  });
});

router.post('/update_user_profile', jsonParser, function (req, res) {
  if(typeof req.headers.secret_key=='undefined')
  {
     result = {
       'statusText': 'false',
       'message': 'Unauthorized access.',
       'status': 401
     };
     res.status(401).json(result);
     return;
  }else
  {
        $query_ch = "SELECT id FROM user WHERE user_token ='" + req.headers.secret_key + "'";
        db.query($query_ch, function (err, query_res, fields) {
        if (err)
        {
          result = {
            'statusText': 'failed',
            'message': 'Something went wrong! please try again later.',
            'status': 500
          };
          res.status(500).json(result);
          return;
        }
        if(query_res.length>0)
        { 
            $query = "UPDATE user SET email_id='"+req.body.email_id+"',name='"+req.body.name+"',gender='"+req.body.gender+"',dob='"+req.body.dob+"',user_image='',country='"+req.body.country+"',state='"+req.body.state+"',city='"+req.body.city+"',address='"+req.body.address+"' WHERE id = '"+query_res[0]['id']+"'";
            db.query($query, function (err, rows, fields) {
            if(err)
            {
              result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
              };
              res.status(500).json(result);
              return;
            }
            if(rows.affectedRows>0)
            { 
              data = {
                'name': req.body.name,
                'email_id': req.body.email_id,
                'mobile_code': query_res[0]['mobile_code'],
                'mobile': query_res[0]['mobile'],
                'dob': req.body.dob,
                'gender': req.body.gender,
                'address': req.body.address,
                'city': req.body.city,
                'state': req.body.state,
                'user_type': query_res[0]['user_type']
              };
              result = {
                'statusText': 'success',
                'message': 'Profile successfully updated.',
                'status': 200
              };
              res.status(200).json(result);
            }else
            {
              result = {
                'statusText': 'failed',
                'message': 'Something went wrong! please try again later.',
                'status': 500
              };
              res.status(500).json(result);
              return;
            }
          });
        }else
        {
          result = {
            'statusText': 'false',
            'message': 'Invalid token',
            'status': 401
          };
          res.status(401).json(result);
          return;
        }
      });
   }  
});

router.post('/update_image',  function (req, res) {

  console.log(req.file.filename);  
  // upload(req, res, function (err) {
  //   if (err) {
  //     // An error occurred when uploading
  //     return res.end("Error uploading file.");
  //   }
  //   console.log(req);
  //   //res.end("File is uploaded");
  //   // Everything went fine
  // })
});


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////      main functions     ////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function truncate_array(arr, l) {
  var new_arr = [];
  for (var k = 0; k < arr.length; k++) {
    if (arr[k].length > l)
      new_arr.push(arr[k].substring(0, l) + '...');
    else
      new_arr.push(arr[k]);
  }
  return new_arr;
}

function seoUrl(string) {

  //Lower case everything
  string = string.toLowerCase();
  //Make alphanumeric (removes all other characters)
  //string = string.replace(/[^a-z0-9+]+/gi, '');
  //Clean up multiple dashes or whitespaces
  string = string.replace(/[^\w ]+/g, '');
  //Convert whitespaces and underscore to dash
  string = string.replace(/ +/g, '-');
  var x = Math.floor((Math.random() * 10000) + 1);
  string = string + x
  return string;
}

function ucfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function random_otp() {
  return val = Math.floor(1000 + Math.random() * 9999);
}

function getRandomInteger() {
  min = Math.ceil(1);
  max = Math.floor(10000);
  return Math.floor(Math.random() * (max - min)) + min;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////       MOBILE APP      ////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

module.exports = router;
