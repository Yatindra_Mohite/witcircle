var express = require('express');
const router = express.Router();
var app = express();
var md5 = require('md5');
var crypto = require('crypto');

// var time = new Date().getTime();
// console.log(parseInt(time / 1000));

var bodyParser = require('body-parser');

var jsonParser = bodyParser.json()
// Parsers for POST data
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

// intercom client
var Intercom = require('intercom-client');
var Iclient = new Intercom.Client({
  token: 'dG9rOmI0Mzg3NjY2XzljZDlfNDMzOV85ZjYxXzU3YTNlNmYzMWIwZjoxOjA='
});
// End intercom client

var mysql = require('mysql');
// var connection = mysql.createConnection({
//   host: 'engineerbabu.c3xvzgfmbihc.us-west-2.rds.amazonaws.com',
//   user: 'portal',
//   password: 'qPcFJRrEeHNh8PWa',
//   database: 'portal'
// });

// var base_url = "https://www.engineerbabu.com/";
//var base_url = "http://localhost:8080/";


var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'portal'
});
var base_url = "http://localhost:3000/";

connection.connect(function (err) {
  if (err) {
    console.log(err.code);
    console.log(err.fatal);
  }
});

var http = require("https");
var urlencode = require('urlencode');


const Razorpay = require('razorpay');

var instance = new Razorpay({
  key_id: 'rzp_live_L3N4UtgpsazQaY',
  key_secret: 'MQdDCU2XkmI808hUh1xu8CRP'
  // key_id: 'rzp_test_85W2bBRkihBjDI',
  // key_secret: 'hAfUem0mgluVhMJPuAiOx2T7'
});

var sg = require('sendgrid')('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');


router.post('/read_all_notifications_of_project', jsonParser, function (req, res) {
  var result = {};
  var image_icon = "assets/img/eb_logo_38x38.jpg";


  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var user_id = rows[0].user_id;
      var project_id = req.body.project_id;

      $query2 = "UPDATE notifications SET status=1 where user_id = '" + user_id + "' AND project_id='" + project_id + "'";
      connection.query($query2, function (err, rows2, fields) {
        if (rows2.affectedRows > 0) {
          $query3 = "select notifications.from_user_id,CONCAT(LEFT(projects.project_title,40),'...') as project_name from notifications LEFT JOIN projects ON notifications.project_id=projects.project_id where notifications.user_id='" + user_id + "'AND notifications.project_id='" + project_id + "'";
          connection.query($query3, function (err, rows3, fields) {
            if (rows3[0]) {
              for (var n1 = 0; n1 < rows3.length; n1++) {

                var project_name = rows3[n1]['project_name'];
                var user_id = rows3[n1]['from_user_id'];
                var notifications_data = JSON.stringify({
                  "notify_type": "normal",
                  "image_icon": image_icon,
                  "message": "Great! <b>" + project_name + "</b> Client has viewed your bid.",
                  "view_btn": 0,
                  "btn_href": ""
                });

                $q7 = "INSERT INTO notifications(user_id,data) VALUES ('" + user_id + "','" + notifications_data + "')";
                connection.query($q7, function (err, rows7, fields) {
                  console.log(rows7);
                  console.log($q7);
                });

              }
            }



          });
          result = {
            'status': 'success'
          };
          res.status(200).json(result);
        }

      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }


  });
});

router.post('/read_chat_notifications', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT user_id,user_type,user_fname FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['user_id']) {

      var user_id = rows[0]['user_id'];

      $query4 = "UPDATE `messages`  SET messages.message_state='1' where  messages.message_state='0' AND messages.mt_id='" + req.body.mt_id + "' AND messages.sender_id!='" + user_id + "'";
      connection.query($query4, function (err, thre_rows, fields) {
        result = {
          'status': 'success',
        };
        res.status(200).json(result);

      });
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
  });
});
router.post('/read_notifications', jsonParser, function (req, res) {
  var result = {};

  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var user_id = rows[0].user_id;
      var id = req.body.id;
      $query2 = "UPDATE notifications SET status=1 where id = '" + id + "'";
      connection.query($query2, function (err, rows2, fields) {
        if (rows2.affectedRows > 0) {
          result = {
            'status': 'success'
          };
          res.status(200).json(result);
        }

      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }


  });
});


router.post('/get_notifications', jsonParser, function (req, res) {
  var result = {};

  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var user_id = rows[0].user_id;
      $query2 = "select * from notifications where user_id='" + user_id + "' AND status=0 order by id DESC";
      connection.query($query2, function (err, rows2, fields) {
        if (rows2[0]) {
          result = {
            'status': 'success',
            'data': rows2
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': []
          };
          res.status(200).json(result);
        }



      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }


  });
});


router.post('/get_awarded_project', jsonParser, function (req, res) {
  var result = {};
  var user_id;
  var awarded_projects = [];

  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "select *,SUBSTRING(project_description,1,150) AS project_desc , (select GROUP_CONCAT(categories.category_name SEPARATOR ',') from project_categories LEFT JOIN categories ON project_categories.category_id = categories.category_id WHERE project_categories.project_id = projects.project_id GROUP BY project_categories.project_id) as expertise from projects where projects.project_awarded_to='" + user_id + "' ORDER BY projects.project_id DESC LIMIT 10";
      connection.query($query2, function (err, award_project, fields) {

        if (award_project[0]) {
          awarded_projects = award_project;
        }

        result = {
          'status': 'success',
          'data': awarded_projects
        };
        res.status(200).json(result);

      });
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
  });
});


router.post('/get_all_portfolio', jsonParser, function (req, res) {
  var result = {};
  var user_id;
  var portfolio = [];

  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query3 = "SELECT freelancers_portfolio.*,categories.category_name from freelancers_portfolio LEFT JOIN categories ON freelancers_portfolio.category_id=categories.category_id WHERE freelancer_id='" + user_id + "'";
      connection.query($query3, function (err, portfolios, fields) {
        if (portfolios[0]) {
          portfolio = portfolios;
        }
        result = {
          'status': 'success',
          'data': portfolio
        };
        res.status(200).json(result);



      });
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
  });
});

router.post('/check_user_for_project', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT user_id,user_fname,user_lname,user_email,user_mobile,user_country FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {

      result = {
        'status': 'success',
        'data': rows[0]
      };
      res.status(200).json(result);


    } else {

      result = {
        'status': 'failed',
        'data': 'user not login'

      };
      res.status(200).json(result);
    }

  });

});

router.post('/send_message', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT user_id,user_type,user_fname,CONCAT(user_fname, ' ' , user_lname) as client_name FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['user_id']) {
      var freelancer_id = req.body.freelancer_id;
      var client_id = rows[0]['user_id'];
      var project_id = req.body.project_id;


      $query1 = "SELECT message_thread.* FROM message_thread where project_id='" + project_id + "' AND  client_id='" + client_id + "' AND freelancer_id='" + freelancer_id + "' LIMIT 1";
      connection.query($query1, function (err, thre_rows, fields) {
        if (thre_rows[0]) {
          //var mt_id =thre_rows[0]['mt_id'];
          result = {
            'status': 'success',
            'data': thre_rows[0]['mt_id']
          };
          res.status(200).json(result);
        } else {
          $query2 = "INSERT INTO message_thread(project_id, client_id, freelancer_id) VALUES('" + project_id + "','" + client_id + "','" + freelancer_id + "')";
          connection.query($query2, function (err, q2_data, fields) {
            if (q2_data.affectedRows > 0) {

              var mt_id = q2_data.insertId;
              $query3 = "SELECT users.user_fname,users.user_email,projects.project_title FROM message_thread INNER JOIN users ON users.user_id=message_thread.freelancer_id INNER JOIN projects ON projects.project_id=message_thread.project_id WHERE mt_id='" + mt_id + "'";
              connection.query($query3, function (err, fi_data, fields) {
                if (fi_data[0]) {

                  var client_name = rows[0]['client_name'];
                  var first_name = fi_data[0]['user_fname'];
                  var email = fi_data[0]['user_email'];
                  var project_title = fi_data[0]['project_title'];

                  var request = sg.emptyRequest({
                    method: 'POST',
                    path: '/v3/mail/send',
                    body: {
                      personalizations: [{
                        to: [{
                          email: email
                        }],
                        subject: 'Engineerbabu: New Conversation started with you',
                        substitutions: {
                          "{user_fname}": first_name,
                          "{project_title}": project_title,
                          "{client_name}": client_name,
                        }
                      }],
                      from: {
                        email: 'no-reply@engineerbabu.com'
                      },
                      template_id: 'e555506c-8b22-42df-89f0-1810e8232cee',
                    }
                  });
                  // With promise
                  sg.API(request)
                    .then(function (response) {
                      // console.log(response.statusCode);
                    })
                    .catch(function (error) {
                      console.log(error.response.statusCode);
                    });

                }
              });



              result = {
                'status': 'success',
                'data': q2_data.insertId
              };
              res.status(200).json(result);
            }
          });
        }
      });
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
  });

});

router.put('/award_project', jsonParser, function (req, res) {
  var result = {};
  var image_icon = "assets/img/eb_logo_38x38.jpg";
  $query = "SELECT user_id,CONCAT(user_fname, ' ' , user_lname) as client_name FROM users WHERE user_auth_token='" + req.headers.secret_key + "'  LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var freelancer_id = req.body.freelancer_id;
      var client_id = rows[0]['user_id'];
      var project_id = req.body.project_id;
      var project_budget = req.body.award_amount;
      var project_timeframe = req.body.timeframe;

      $query1 = "SELECT project_id,client_id,project_alias,project_title,CONCAT(LEFT(project_title,40),'...') as project_title2 FROM projects WHERE project_id='" + project_id + "' AND project_state='0' AND client_id='" + client_id + "' LIMIT 1";
      connection.query($query1, function (err, pro_rows, fields) {
        if (pro_rows[0]) {
          $query2 = "SELECT user_id,user_email,user_fname FROM users WHERE user_id='" + freelancer_id + "' AND user_type!='1'";
          connection.query($query2, function (err, check_user, fields) {
            if (check_user[0]['user_id']) {
              var first_name = check_user[0]['user_fname'];
              var email = check_user[0]['user_email'];
              var client_name = rows[0]['client_name'];
              var project_title = pro_rows[0]['project_title'];


              $query4 = "UPDATE projects SET project_awarded_amount='" + project_budget + "',project_awarded_days='" + project_timeframe + "',project_state='1',project_awarded_to='" + freelancer_id + "',project_awarded_on=CURRENT_TIMESTAMP WHERE project_id='" + project_id + "' AND project_state='0' AND client_id='" + client_id + "'";
              connection.query($query4, function (err, insert_award, fields) {
                if (insert_award.affectedRows > 0) {


                  var request = sg.emptyRequest({
                    method: 'POST',
                    path: '/v3/mail/send',
                    body: {
                      personalizations: [{
                        to: [{
                          email: email
                        }],
                        subject: 'EngineerBabu: Yayyy!! You have been Awarded a Project',
                        substitutions: {
                          "{user_fname}": first_name,
                          "{project_title}": project_title,
                          "{client_name}": client_name,
                        }
                      }],
                      from: {
                        email: 'no-reply@engineerbabu.com'
                      },
                      template_id: 'f0e7745e-2380-4b14-81c5-4a60c09523d0',
                    }
                  });
                  // With promise
                  sg.API(request)
                    .then(function (response) {
                      // console.log(response.statusCode);
                    })
                    .catch(function (error) {

                      // console.log(error.response.statusCode);
                    });

                  var project_name = pro_rows[0]['project_title2'];
                  var client_id = pro_rows[0].client_id;
                  var project_alias = pro_rows[0].project_alias;
                  var project_id = pro_rows[0].project_id;
                  var notifications_data = JSON.stringify({
                    "notify_type": "award_project",
                    "image_icon": image_icon,
                    "message": "Congrats! <b>" + project_name + "</b> is awarded to you.",
                    "view_btn": 1,
                    "btn_href": project_alias
                  });

                  $q7 = "INSERT INTO notifications(user_id,from_user_id,project_id,data) VALUES ('" + freelancer_id + "','" + client_id + "','" + project_id + "','" + notifications_data + "')";
                  connection.query($q7, function (err, rows7, fields) {
                    // console.log($q7);
                    // console.log(rows7);
                  });

                  result = {
                    'status': 'success',
                    'data': 'awarded successfully'
                  };
                  res.status(200).json(result);
                } else {
                  result = {
                    'status': 'failed',
                    'data': 'something wrong'
                  };
                  res.status(200).json(result);
                }
              });
            } else {
              result = {
                'status': 'failed',
                'data': 'freelancer is not valid'
              };
              res.status(200).json(result);
            }
          });
        } else {
          result = {
            'status': 'failed',
            'data': 'project is not open for hire'
          };
          res.status(200).json(result);
        }
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'unauthorised access'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

function truncate_array(arr, l) {
  var new_arr = [];

  for (var k = 0; k < arr.length; k++) {
    if (arr[k].length > l)
      new_arr.push(arr[k].substring(0, l) + '...');
    else
      new_arr.push(arr[k]);
  }
  return new_arr;

}

router.post('/get_phonecode_from_countries', jsonParser, function (req, res) {
  var result = {};

  if (typeof req.body.iso_name == 'undefined' || req.body.iso_name == null || req.body.iso_name == '') {
    result = {
      'status': 'failed'
    };
    res.status(200).json(result);
    return;
  }
  var iso_name = req.body.iso_name;

  $query = "SELECT phonecode FROM countries where iso='" + iso_name + "'";

  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows[0].phonecode
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'country not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});


router.get('/getDirectory', jsonParser, function (req, res) {
  var result = {};
  //$query = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',4) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,(select count(freelancers_skills.fskill_id) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id) as skills_count,(select count(freelancers_portfolio.fp_id) from freelancers_portfolio WHERE freelancers_portfolio.freelancer_id = users.user_id) as portfolio_count,(select view_count FROM user_profile_view WHERE user_id = users.user_id) as profile_count FROM `users` WHERE `user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_id IN (SELECT freelancers_portfolio.freelancer_id FROM freelancers_portfolio ) AND users.user_id IN (SELECT freelancers_skills.freelancer_id FROM freelancers_skills ) AND users.user_id IN (SELECT freelancers_categories.freelancer_id FROM freelancers_categories ) ORDER BY users.user_profile_completed DESC LIMIT 12";
  $query = "SELECT users.user_fname,users.user_lname,users.user_city,CONCAT(LEFT(users.user_about,400),'...') as user_about,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users` Left JOIN user_state ON users.user_id=user_state.user_id WHERE `user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' and users.user_about!='' AND user_state.portfolio_count > 0 AND user_state.category_count > 0 AND user_state.skill_count > 0 having CHAR_LENGTH(user_about)>100 ORDER BY users.user_profile_completed DESC LIMIT 24";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var skill_name = [];
      for (var i = 0; i < rows.length; i++) {
        skill_name = rows[i].skills_name.split(', ');
        if (skill_name.length > 1) {
          rows[i].skill_arr = truncate_array(skill_name, 10);
        } else {
          rows[i].skill_arr = skill_name;
        }

        rows[i].skill_full_arr = skill_name;
        // expertise_name = rows[i].expertise_images.split(', ');
        // var exp_icon= [];
        // var expertise = [];
        // var a = [];
        // for(j=0;j<expertise_name.length; j++){
        //     a=expertise_name[j].split('_');
        //     expertise.push(a[0]);
        //     exp_icon.push(a[1]);
        // }
        //  rows[i].expertise_arr = expertise;
        //  rows[i].expertise_image_arr = exp_icon;
      }

      result = {
        'status': 'success',
        'data': rows,
        'count': rows.length
      };
      res.status(200).json(result);


      //end forloop
    } else {
      result = {
        'status': 'failed',
        'data': 'No results found.',
        'count': rows.length
      };
      res.status(200).json(result);
    }

  });

});

router.post('/getDirectoryLoadMore', jsonParser, function (req, res) {
  var result = {};
  var offset = req.body.offset;
  if (typeof offset == 'undefined') {
    result = {
      'status': 'failed',
      'data': 'Boom! Unauthorized Access.'
    };
    res.status(200).json(result);
    return;
  }
  var search_city = '';
  var city_filter = '';

  var category = '';
  var cate_filter = "";
  var cate_join = "";

  var skill_arr = [];
  var skill_filter = "";
  var skill_join = "";
  var user_filter = "";
  var search_user = "";

  if (typeof req.body.searchdata != 'undefined') {

    if (req.body.searchdata.city_name != '') {
      search_city = req.body.searchdata.city_name;
    }

    if (search_city != '') {
      city_filter = "AND users.user_city LIKE '%" + search_city + "%'";
    }


    if (req.body.searchdata.developer_name != "") {
      search_user = req.body.searchdata.developer_name;
      user_filter = "AND (users.user_fname LIKE '%" + search_user + "%' OR users.user_lname LIKE '%" + search_user + "%' OR users.user_company_name LIKE '%" + search_user + "%')";
    }


    if (req.body.searchdata.category != '') {
      category = req.body.searchdata.category;
    }



    if (category != '') {
      cate_filter = cate_filter + "freelancers_categories.category_id = '" + category + "'";
      cate_filter = " AND (" + cate_filter + ")";
      cate_join = "LEFT JOIN freelancers_categories ON freelancers_categories.freelancer_id=users.user_id";
    }

    if (req.body.searchdata.skill_arr.length > 0) {
      skill_arr = req.body.searchdata.skill_arr;

      for (var i = 0; i < skill_arr.length; i++) {
        if (i == 0) {
          skill_filter += "freelancers_skills.skill_id = '" + skill_arr[i].skill_id + "' ";
        } else {
          skill_filter += " OR freelancers_skills.skill_id = '" + skill_arr[i].skill_id + "' ";
        }

      }
      skill_filter = " AND (" + skill_filter + ")";
      skill_join = "LEFT JOIN freelancers_skills ON users.user_id=freelancers_skills.freelancer_id";

    }

  }

  //$query = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',5) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,(select count(freelancers_skills.fskill_id) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id) as skills_count,(select count(freelancers_portfolio.fp_id) from freelancers_portfolio WHERE freelancers_portfolio.freelancer_id = users.user_id) as portfolio_count,(select view_count FROM user_profile_view WHERE user_id = users.user_id) as profile_count FROM `users` "+skill_join+" "+cate_join+"  WHERE `user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' "+skill_filter+" "+cate_filter+ " "+ city_filter + " "+user_filter+" AND users.user_id IN (SELECT freelancers_portfolio.freelancer_id FROM freelancers_portfolio )  AND users.user_id IN (SELECT freelancers_skills.freelancer_id FROM freelancers_skills ) AND users.user_id IN (SELECT freelancers_categories.freelancer_id FROM freelancers_categories ) ORDER BY users.user_id DESC LIMIT "+offset+" ,12";
  $query = "SELECT users.user_fname,users.user_lname,users.user_city,CONCAT(LEFT(users.user_about,400),'...') as user_about,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users` Left JOIN user_state ON users.user_id=user_state.user_id  " + skill_join + " " + cate_join + "  WHERE `user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' " + skill_filter + " " + cate_filter + " " + city_filter + " " + user_filter + " AND users.user_about!='' AND user_state.portfolio_count > 0 AND user_state.category_count > 0 AND user_state.skill_count > 0 having CHAR_LENGTH(user_about)>100 ORDER BY users.user_id DESC LIMIT " + offset + " ,24";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var skill_name = [];
      for (var i = 0; i < rows.length; i++) {
        skill_name = rows[i].skills_name.split(', ');


        if (skill_name.length > 1) {
          rows[i].skill_arr = truncate_array(skill_name, 10);
        } else {
          rows[i].skill_arr = skill_name;
        }
        rows[i].skill_full_arr = skill_name;
        // expertise_name = rows[i].expertise_images.split(', ');
        // var exp_icon= [];
        // var expertise = [];
        // var a = [];
        // for(j=0;j<expertise_name.length; j++){
        //     a=expertise_name[j].split('_');
        //     expertise.push(a[0]);
        //     exp_icon.push(a[1]);
        // }
        //  rows[i].expertise_arr = expertise;
        //  rows[i].expertise_image_arr = exp_icon;
      }

      result = {
        'status': 'success',
        'data': rows,
        'count': rows.length
      };
      res.status(200).json(result);


      //end forloop
    } else {
      result = {
        'status': 'failed',
        'data': 'No results found.',
        count: rows.length
      };
      res.status(200).json(result);
    }

  });

});

router.post('/getDirectorySearch', jsonParser, function (req, res) {
  var result = {};

  var search_city = '';
  var city_filter = '';

  var category = '';
  var cate_filter = "";
  var cate_join = "";

  var skill_arr = [];
  var skill_filter = "";
  var skill_join = "";
  var user_filter = "";
  var search_user = "";

  if (typeof req.body.searchdata != 'undefined') {

    if (req.body.searchdata.city_name != '') {
      search_city = req.body.searchdata.city_name;
    }

    if (search_city != '') {
      city_filter = "AND users.user_city LIKE '%" + search_city + "%'";
    }


    if (req.body.searchdata.developer_name != '') {
      search_user = req.body.searchdata.developer_name;
      user_filter = "AND (users.user_fname LIKE '%" + search_user + "%' OR users.user_lname LIKE '%" + search_user + "%' OR users.user_company_name LIKE '%" + search_user + "%')";
    }


    if (req.body.searchdata.category != '') {
      category = req.body.searchdata.category;
    }

    if (category != '') {
      cate_filter = cate_filter + "freelancers_categories.category_id = '" + category + "'";
      cate_filter = " AND (" + cate_filter + ")";
      cate_join = "LEFT JOIN freelancers_categories ON freelancers_categories.freelancer_id=users.user_id";
    }

    if (req.body.searchdata.skill_arr.length > 0) {
      skill_arr = req.body.searchdata.skill_arr;

      for (var i = 0; i < skill_arr.length; i++) {
        if (i == 0) {
          skill_filter += "freelancers_skills.skill_id = '" + skill_arr[i].skill_id + "' ";
        } else {
          skill_filter += " OR freelancers_skills.skill_id = '" + skill_arr[i].skill_id + "' ";
        }

      }
      skill_filter = " AND (" + skill_filter + ")";
      skill_join = "LEFT JOIN freelancers_skills ON users.user_id=freelancers_skills.freelancer_id";

    }

  }


  //$query = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',4) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,(select count(freelancers_skills.fskill_id) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id) as skills_count,(select count(freelancers_portfolio.fp_id) from freelancers_portfolio WHERE freelancers_portfolio.freelancer_id = users.user_id) as portfolio_count,(select view_count FROM user_profile_view WHERE user_id = users.user_id) as profile_count FROM `users` "+skill_join+" "+cate_join+ " WHERE `user_email_confirmed` = 1 AND `user_username`!='' AND `user_image`!= '' AND users.user_status!='2' AND users.user_privacy='0' " +skill_filter+" " +cate_filter+ " " +city_filter+ " "+user_filter+" AND users.user_id IN (SELECT freelancers_portfolio.freelancer_id FROM freelancers_portfolio ) AND users.user_id IN (SELECT freelancers_skills.freelancer_id FROM freelancers_skills ) AND users.user_id IN (SELECT freelancers_categories.freelancer_id FROM freelancers_categories ) ORDER BY users.user_id DESC LIMIT 12";
  $query = "SELECT users.user_fname,users.user_lname,users.user_city,users.user_image,CONCAT(LEFT(users.user_about,400),'...') as user_about, users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users`  Left JOIN user_state ON users.user_id=user_state.user_id " + skill_join + " " + cate_join + " WHERE `user_email_confirmed` = 1 AND `user_username`!='' AND `user_image`!= '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_about!='' " + skill_filter + " " + cate_filter + " " + city_filter + " " + user_filter + " AND user_state.portfolio_count > 0 AND user_state.category_count > 0 AND user_state.skill_count > 0 ORDER BY users.user_id DESC LIMIT 12";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var skill_name = [];
      for (var i = 0; i < rows.length; i++) {
        skill_name = rows[i].skills_name.split(', ');

        if (skill_name.length > 1) {
          rows[i].skill_arr = truncate_array(skill_name, 10);
        } else {
          rows[i].skill_arr = skill_name;
        }
        rows[i].skill_full_arr = skill_name;
        // expertise_name = rows[i].expertise_images.split(', ');
        // var exp_icon= [];
        // var expertise = [];
        // var a = [];
        // for(j=0;j<expertise_name.length; j++){
        //     a=expertise_name[j].split('_');
        //     expertise.push(a[0]);
        //     exp_icon.push(a[1]);
        // }
        //  rows[i].expertise_arr = expertise;
        //  rows[i].expertise_image_arr = exp_icon;
      }

      result = {
        'status': 'success',
        'data': rows,
        'count': rows.length,
        'q': $query
      };
      res.status(200).json(result);


      //end forloop
    } else {
      result = {
        'status': 'failed',
        'data': 'No results found.',
        count: rows.length,
        'q': $query
      };
      res.status(200).json(result);
    }

  });

});

router.post('/getDirectoryByKeyword', jsonParser, function (req, res) {
  var result = {};

  var last_url = req.body.last_url;
  var url_length = last_url.length;

  var cate_join = "";
  var skill_join = "";
  var cate_skill_where = "";
  var city_where = "";
  var meta_skill = "";
  var meta_city = "AND meta_tags.city=''";
  var meta_tags = "";


  if (typeof last_url != "undefined" || last_url != "" || last_url != null) {

    last_url = last_url.replace('designers', 'developers');
    last_url = last_url.replace('expert', 'developers');
    last_url = last_url.replace('writer', 'developers');

    if (last_url.indexOf("developers") !== -1) {
      var pro_from = 0;
      var pro_to = last_url.indexOf("-developers");
      var skill = last_url.substr(pro_from, pro_to).toLowerCase();
      // exp_arr = pro_ty.split('-');
      cate_join = "LEFT JOIN freelancers_categories ON users.user_id=freelancers_categories.freelancer_id LEFT JOIN categories ON freelancers_categories.category_id=categories.category_id";
      skill_join = "LEFT JOIN freelancers_skills ON users.user_id=freelancers_skills.freelancer_id LEFT JOIN skills ON freelancers_skills.skill_id=skills.skill_id ";
      cate_skill_where = "(categories.category_name= '" + skill + "' OR skills.skill_name= '" + skill + "') AND";
      meta_skill = "meta_tags.skills= '" + skill + "'";
    }

    if (last_url.indexOf("-in-") !== -1) {
      var pro_from = 0;
      var city_fr = last_url.indexOf("-in-");
      var city = last_url.substr(city_fr + 4, url_length).toLowerCase();
      city_where = "users.user_city= '" + city + "' AND";
      meta_city = "AND meta_tags.city='" + city + "'";
    }
  }


  //$query = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',4) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,(select count(freelancers_skills.fskill_id) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id) as skills_count,(select count(freelancers_portfolio.fp_id) from freelancers_portfolio WHERE freelancers_portfolio.freelancer_id = users.user_id) as portfolio_count,(select view_count FROM user_profile_view WHERE user_id = users.user_id) as profile_count FROM `users` "+cate_join+" "+skill_join+" WHERE "+cate_skill_where+" "+city_where+" `user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_id IN (SELECT freelancers_portfolio.freelancer_id FROM freelancers_portfolio ) AND users.user_id IN (SELECT freelancers_skills.freelancer_id FROM freelancers_skills ) AND users.user_id IN (SELECT freelancers_categories.freelancer_id FROM freelancers_categories ) GROUP BY users.user_id ORDER BY rand() LIMIT 12";
  $query = "SELECT users.user_fname,users.user_lname,users.user_city,CONCAT(LEFT(users.user_about,400),'...') as user_about,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users` Left JOIN user_state ON users.user_id=user_state.user_id " + cate_join + " " + skill_join + " WHERE " + cate_skill_where + " " + city_where + "`user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_about!='' AND user_state.portfolio_count > 0 AND user_state.category_count > 0 AND user_state.skill_count > 0 GROUP BY users.user_id ORDER BY rand() LIMIT 60";


  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'

      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var skill_name = [];
      for (var i = 0; i < rows.length; i++) {
        skill_name = rows[i].skills_name.split(', ');
        if (skill_name.length > 1) {
          rows[i].skill_arr = truncate_array(skill_name, 10);
        } else {
          rows[i].skill_arr = skill_name;
        }
        rows[i].skill_full_arr = skill_name;

      }

      $query2 = "select * from meta_tags where " + meta_skill + " " + meta_city + "";
      connection.query($query2, function (err, rows2, fields) {

        if (rows2[0]) {
          meta_tags = rows2;
        }
        result = {
          'status': 'success',
          'data': rows,
          'count': rows.length,
          'meta_tags': meta_tags
        };
        res.status(200).json(result);
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'No results found.',
        'count': rows.length
      };
      res.status(200).json(result);
    }

  });

});

router.post('/getDirectoryByCountry', jsonParser, function (req, res) {
  var result = {};

  var last_url = req.body.last_url;
  var url_length = last_url.length;

  var cate_join = "";
  var skill_join = "";
  var cate_skill_where = "";
  var city_where = "";
  // var meta_skill = "";
  // var meta_city = "AND meta_tags.city=''";
  // var meta_tags = "";


  if (typeof last_url != "undefined" || last_url != "" || last_url != null) {

    // last_url = last_url.replace('designers', 'developers');
    // last_url = last_url.replace('expert', 'developers');
    // last_url = last_url.replace('writer', 'developers');

    // if (last_url.indexOf("developers") !== -1) {
    //   var pro_from = 0;
    //   var pro_to = last_url.indexOf("-developers");
    //   var skill = last_url.substr(pro_from, pro_to).toLowerCase();
    //   // exp_arr = pro_ty.split('-');
    //   cate_join = "LEFT JOIN freelancers_categories ON users.user_id=freelancers_categories.freelancer_id LEFT JOIN categories ON freelancers_categories.category_id=categories.category_id";
    //   skill_join = "LEFT JOIN freelancers_skills ON users.user_id=freelancers_skills.freelancer_id LEFT JOIN skills ON freelancers_skills.skill_id=skills.skill_id ";
    //   cate_skill_where = "(categories.category_name= '" + skill + "' OR skills.skill_name= '" + skill + "') AND";
    //   //meta_skill = "meta_tags.skills= '" + skill + "'";
    // }

    if (last_url.indexOf("-in-") !== -1) {
      var pro_from = 0;
      var country_fr = last_url.indexOf("-in-");
      var country = last_url.substr(country_fr + 4, url_length).toLowerCase();
      if (country == 'united-arab-emirates') country = 'AE';
      else if (country == 'australia') country = 'AU';
      else if (country == 'united-kingdom') country = 'GB';
      else if (country == 'ukraine') country = 'UA';
      else if (country == 'united-states') country = 'US';
      else if (country == 'singapore') country = 'SG';
      else if (country == 'egypt') country = 'EG';
      else if (country == 'canada') country = 'CA';
      else if (country == 'germany') country = 'DE';
      else if (country == 'philippines') country = 'PH';
      else if (country == 'south-africa') country = 'ZA';
      else country = 'JP';

      country_where = "users.user_country= '" + country + "' AND";
      //meta_country = "AND meta_tags.country='" + country + "'";
    }
  }


  //$query = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,CONCAT(LEFT(users.user_about,400),'...') as user_about,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users` Left JOIN user_state ON users.user_id=user_state.user_id WHERE " + country_where + "`user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_about!='' AND user_state.portfolio_count > 0 AND user_state.category_count > 0 AND user_state.skill_count > 0 GROUP BY users.user_id ORDER BY rand() LIMIT 60";
  $query = "SELECT users.user_fname,users.user_lname,users.user_city,CONCAT(LEFT(users.user_about,400),'...') as user_about,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users` Left JOIN user_state ON users.user_id=user_state.user_id WHERE " + country_where + "`user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_about!=''  GROUP BY users.user_id having CHAR_LENGTH(user_about)>30 ORDER BY rand() LIMIT 60";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'

      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var skill_name = [];

      for (var i = 0; i < rows.length; i++) {
        if (rows[i].skills_name != null) {
          skill_name = rows[i].skills_name.split(', ');
          if (skill_name.length > 1) {
            rows[i].skill_arr = truncate_array(skill_name, 10);
          } else {
            rows[i].skill_arr = skill_name;
          }
          rows[i].skill_full_arr = skill_name;
        }
      }


      // $query2 = "select * from meta_tags where " + meta_skill + " " + meta_city + "";
      // connection.query($query2, function (err, rows2, fields) {

      //   if (rows2[0]) {
      //     meta_tags = rows2;
      //   }

      // });

      result = {
        'status': 'success',
        'data': rows,
        'count': rows.length
        // 'meta_tags': meta_tags
      };
      res.status(200).json(result);

    } else {
      result = {
        'status': 'failed',
        'data': 'No results found.',
        'count': rows.length
      };
      res.status(200).json(result);
    }

  });

});

router.post('/getRelatedUsers', jsonParser, function (req, res) {
  var result = {};
  var user_city = "";
  var user_name = req.body.user_name;
  if (req.body.user_city != '') {
    user_city = req.body.user_city;
    $query = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,CONCAT(LEFT(users.user_about,400),'...') as user_about,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users` Left JOIN user_state ON users.user_id=user_state.user_id WHERE " + "`user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_about!='' AND users.user_city='" + user_city + "' AND users.user_username!='" + user_name + "' AND user_state.portfolio_count > 0 AND user_state.category_count > 0  AND user_state.skill_count > 0 GROUP BY users.user_id having CHAR_LENGTH(user_about)>100 ORDER BY rand() LIMIT 4";
  } else {
    $query = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,CONCAT(LEFT(users.user_about,400),'...') as user_about,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users` Left JOIN user_state ON users.user_id=user_state.user_id WHERE " + "`user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_about!='' AND users.user_username!='" + user_name + "' AND user_state.portfolio_count > 0 AND user_state.category_count > 0 AND user_state.skill_count > 0  GROUP BY users.user_id having CHAR_LENGTH(user_about)>100 ORDER BY rand() LIMIT 4";
  }

  //$query = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',4) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,(select count(freelancers_skills.fskill_id) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id) as skills_count,(select count(freelancers_portfolio.fp_id) from freelancers_portfolio WHERE freelancers_portfolio.freelancer_id = users.user_id) as portfolio_count,(select view_count FROM user_profile_view WHERE user_id = users.user_id) as profile_count FROM `users` "+cate_join+" "+skill_join+" WHERE "+cate_skill_where+" "+city_where+" `user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_id IN (SELECT freelancers_portfolio.freelancer_id FROM freelancers_portfolio ) AND users.user_id IN (SELECT freelancers_skills.freelancer_id FROM freelancers_skills ) AND users.user_id IN (SELECT freelancers_categories.freelancer_id FROM freelancers_categories ) GROUP BY users.user_id ORDER BY rand() LIMIT 12";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {

      var skill_name = [];
      for (var i = 0; i < rows.length; i++) {
        skill_name = rows[i].skills_name.split(', ');
        if (skill_name.length > 1) {
          rows[i].skill_arr = truncate_array(skill_name, 10);
        } else {
          rows[i].skill_arr = skill_name;
        }
        rows[i].skill_full_arr = skill_name;
      }
      result = {
        'status': 'success',
        'data': rows,
        'count': rows.length
      };
      res.status(200).json(result);

    } else {
      $query2 = "SELECT users.user_id,users.user_fname,users.user_lname,users.user_city,CONCAT(LEFT(users.user_about,400),'...') as user_about,users.user_image,users.image_handler,users.user_username,users.user_type,users.user_company_name,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,(select substring_index(GROUP_CONCAT(skills.skill_name SEPARATOR ', '),',',8) from freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id = skills.skill_id WHERE freelancers_skills.freelancer_id = users.user_id GROUP BY freelancers_skills.freelancer_id) as skills_name,user_state.skill_count as skills_count,user_state.portfolio_count,user_state.profile_view_count as profile_count FROM `users` Left JOIN user_state ON users.user_id=user_state.user_id WHERE " + "`user_email_confirmed` = 1  AND `user_username` != '' AND `user_image` != '' AND users.user_status!='2' AND users.user_privacy='0' AND users.user_about!='' AND users.user_username!='" + user_name + "' AND user_state.portfolio_count > 0 AND user_state.category_count > 0 AND user_state.skill_count > 0  GROUP BY users.user_id having CHAR_LENGTH(user_about)>100 ORDER BY rand() LIMIT 4";
      connection.query($query2, function (err, rows2, fields) {
        if (rows2[0]) {

          var skill_name = [];
          for (var i = 0; i < rows2.length; i++) {
            skill_name = rows2[i].skills_name.split(', ');
            if (skill_name.length > 1) {
              rows2[i].skill_arr = truncate_array(skill_name, 10);
            } else {
              rows2[i].skill_arr = skill_name;
            }
            rows2[i].skill_full_arr = skill_name;
          }
          result = {
            'status': 'success',
            'data': rows2,
            'count': rows2.length
          };
          res.status(200).json(result);

        } else {
          result = {
            'status': 'failed',
            'data': 'No results found.',
            'count': rows2.length
          };
          res.status(200).json(result);
        }
      });
    }

  });

});

router.get('/getChatThread', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT user_id,user_type,user_fname FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['user_id']) {

      var user_id = rows[0]['user_id'];
      var user_type = rows[0]['user_type'];

      if (user_type == 1) {
        var where = "WHERE message_thread.client_id='" + user_id + "'";


        $query1 = "SELECT (SELECT count(*) FROM messages where messages.message_state='0' AND messages.mt_id=message_thread.mt_id AND messages.sender_id!='" + user_id + "') as unread_msg,message_thread.*,projects.project_title,projects.project_description,users.user_fname,users.user_lname,users.user_image,users.user_auth_token,case users.is_online when '1' then 'online' when '0' then '' end as is_online FROM message_thread LEFT JOIN projects ON message_thread.project_id=projects.project_id LEFT JOIN users ON message_thread.freelancer_id=users.user_id " + where + " ORDER BY message_thread.mt_id DESC";
        //console.log($query1);
        connection.query($query1, function (err, thre_rows, fields) {
          result = {
            'status': 'success',
            'data': thre_rows,
            'user_data': rows[0]
          };
          res.status(200).json(result);

        });

      } else {
        var where = "WHERE message_thread.freelancer_id='" + user_id + "'";
        $query1 = "SELECT (SELECT count(*) FROM messages where messages.message_state='0' AND messages.mt_id=message_thread.mt_id AND messages.sender_id!='" + user_id + "') as unread_msg,message_thread.*,projects.project_title,projects.project_description,users.user_fname,users.user_lname,users.user_image,users.user_auth_token,case users.is_online when '1' then 'online' when '0' then '' end as is_online FROM message_thread LEFT JOIN projects ON message_thread.project_id=projects.project_id LEFT JOIN users ON message_thread.client_id=users.user_id " + where + " ORDER BY message_thread.mt_id DESC";

        connection.query($query1, function (err, thre_rows, fields) {
          result = {
            'status': 'success',
            'data': thre_rows,
            'user_data': rows[0]
          };
          res.status(200).json(result);

        });
      }

      //result ={'status':'success','data':rows[0]};
      //res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});
router.get('/getChatThreadMessages', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT user_id,user_type,user_fname FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['user_id']) {

      var user_id = rows[0]['user_id'];
      var user_type = rows[0]['user_type'];


      if (user_type == 1) {
        $query4 = "UPDATE `messages` LEFT JOIN message_thread ON messages.mt_id=message_thread.mt_id SET messages.message_state='1' where message_thread.client_id='" + user_id + "' AND messages.message_state='0' AND messages.mt_id='" + req.query.mt_id + "' AND messages.sender_id!='" + user_id + "'";
        connection.query($query4, function (err, thre_rows, fields) {});

      } else {
        $query4 = "UPDATE `messages` LEFT JOIN message_thread ON messages.mt_id=message_thread.mt_id SET messages.message_state='1' where message_thread.freelancer_id='" + user_id + "' AND messages.message_state='0' AND messages.mt_id='" + req.query.mt_id + "' AND messages.sender_id!='" + user_id + "'";
        connection.query($query4, function (err, thre_rows, fields) {});
      }


      $query1 = "SELECT messages.*,users.user_type,users.user_fname,users.user_image,case users.is_online when '1' then 'online' when '0' then '' end as is_online FROM messages LEFT JOIN users ON messages.sender_id=users.user_id WHERE messages.mt_id='" + req.query.mt_id + "' ORDER BY message_id ASC";
      connection.query($query1, function (err, thre_rows, fields) {
        result = {
          'status': 'success',
          'data': thre_rows
        };
        res.status(200).json(result);

      });
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
  });

});

router.get('/getChatThreadNotification', jsonParser, function (req, res) {
  var result = {};
  var final_array = [];

  $query = "SELECT user_id,user_type,user_fname FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['user_id']) {

      var user_id = rows[0]['user_id'];
      var user_type = rows[0]['user_type'];

      if (user_type == 1) {
        $query1 = "SELECT mt.mt_id,projects.project_title,users.image_handler,users.user_fname,(select count(*)from messages where messages.mt_id=mt.mt_id and message_state='0' and messages.sender_id!='" + user_id + "') as unread_count FROM message_thread mt INNER JOIN projects ON mt.project_id=projects.project_id  LEFT JOIN users ON mt.freelancer_id=users.user_id WHERE mt.client_id='" + user_id + "' order by mt.mt_id desc";
        //console.log($query1);
        connection.query($query1, function (err, thre_rows, fields) {
          thre_rows.forEach(function (entry) {
            if (entry.unread_count > 0) {
              final_array.push(entry)
            }
          })
          result = {
            'status': 'success',
            'data': final_array,
          };
          res.status(200).json(result);

        });

      } else {
        $query1 = "SELECT mt.mt_id,projects.project_title,users.image_handler,users.user_fname,(select count(*)from messages where messages.mt_id=mt.mt_id and message_state='0' and messages.sender_id!='" + user_id + "') as unread_count FROM message_thread mt INNER JOIN projects ON mt.project_id=projects.project_id  LEFT JOIN users ON mt.client_id=users.user_id  WHERE mt.freelancer_id='" + user_id + "' order by mt.mt_id desc";
        connection.query($query1, function (err, thre_rows, fields) {
          thre_rows.forEach(function (entry) {
            if (entry.unread_count > 0) {
              final_array.push(entry)
            }
          })
          result = {
            'status': 'success',
            'data': final_array,
          };
          res.status(200).json(result);

        });
      }

      //result ={'status':'success','data':rows[0]};
      //res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});


// router.get('/getChatThread',jsonParser,function (req, res)  {
//     var result = {};
//     $query = "SELECT user_id,user_type,user_fname FROM users WHERE user_auth_token='"+req.headers.secret_key+"' LIMIT 1";

//     connection.query($query, function(err, rows, fields) {
//     if(err){
//       result={'status':'failed','data':'An error ocurred while performing the query.'};
//       res.status(500).json(result);
//     return;
//     }
//     if(rows[0]['user_id']){

//       var user_id = rows[0]['user_id'];
//       var user_type = rows[0]['user_type'];

//       if(user_type==1){
//           var where = "WHERE message_thread.client_id='"+user_id+"'";


//           $query1 = "SELECT message_thread.*,projects.project_title,projects.project_description,users.user_fname,users.user_lname,users.user_image,users.user_auth_token,case users.is_online when '1' then 'online' when '0' then '' end as is_online FROM message_thread LEFT JOIN projects ON message_thread.project_id=projects.project_id LEFT JOIN users ON message_thread.freelancer_id=users.user_id "+where+" ORDER BY message_thread.mt_id DESC";

//           connection.query($query1, function(err, thre_rows, fields) {
//             result ={'status':'success','data':thre_rows,'user_data':rows[0]};
//             res.status(200).json(result);

//           });

//       }else{
//           var where = "WHERE message_thread.freelancer_id='"+user_id+"'";
//           //console.log(where);
//           $query1 = "SELECT message_thread.*,projects.project_title,projects.project_description,users.user_fname,users.user_lname,users.user_image,users.user_auth_token,case users.is_online when '1' then 'online' when '0' then '' end as is_online FROM message_thread LEFT JOIN projects ON message_thread.project_id=projects.project_id LEFT JOIN users ON message_thread.client_id=users.user_id "+where+" ORDER BY message_thread.mt_id DESC";

//           connection.query($query1, function(err, thre_rows, fields) {
//             result ={'status':'success','data':thre_rows,'user_data':rows[0]};
//             res.status(200).json(result);

//           });
//       }

//       //result ={'status':'success','data':rows[0]};
//       //res.status(200).json(result);
//     }else{
//       result ={'status':'failed','data':'user not available'};
//       res.status(200).json(result);
//     }
//     //console.log("Query succesfully executed: ", rows);
//     });

// });
// router.get('/getChatThreadMessages',jsonParser,function (req, res)  {
//     var result = {};
//     $query = "SELECT user_id,user_type,user_fname FROM users WHERE user_auth_token='"+req.headers.secret_key+"' LIMIT 1";

//     connection.query($query, function(err, rows, fields) {
//     if(err){
//       result={'status':'failed','data':'An error ocurred while performing the query.'};
//       res.status(500).json(result);
//     return;
//     }
//     if(rows[0]['user_id']){

//       var user_id = rows[0]['user_id'];
//       var user_type = rows[0]['user_type'];


//       $query1 = "SELECT messages.*,users.user_type,users.user_fname,users.user_image,case users.is_online when '1' then 'online' when '0' then '' end as is_online FROM messages LEFT JOIN users ON messages.sender_id=users.user_id WHERE messages.mt_id='"+req.query.mt_id+"' ORDER BY message_id ASC";
//       connection.query($query1, function(err, thre_rows, fields) {
//         result ={'status':'success','data':thre_rows};
//         res.status(200).json(result);

//       });
//     }else{
//       result ={'status':'failed','data':'user not available'};
//       res.status(200).json(result);
//     }
//     });

// });

router.get('/check_token', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT user_id,user_type,user_auth_token,user_reg_status FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['user_id']) {
      result = {
        'status': 'success',
        'data': rows[0]
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
  });
});

router.post('/project_edit', jsonParser, function (req, res) {
  var result = {};
  var project_alias = req.body.project_alias;

  var user_id;
  $query1 = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query1, function (err, user_rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (user_rows[0]) {
      user_id = user_rows[0]['user_id'];
      //project_alias = seoUrl(req.body.project_title);

      $query2 = "Update projects SET project_title='" + req.body.project_title.replace(/'/g, "") + "',project_description='" + req.body.description.replace(/'/g, "") + "',project_budget='" + req.body.budget + "',project_timeframe='" + req.body.timeframe + "' WHERE project_alias='" + project_alias + "'";

      connection.query($query2, function (err, project, fields) {
        if (project.affectedRows > 0) {

          $query3 = "Select project_id from projects where project_alias = '" + project_alias + "'";
          connection.query($query3, function (err, project_row, fields) {
            if (project_row[0]) {
              var project_id = project_row[0].project_id;

              $query2 = "Delete from project_categories WHERE project_id='" + project_id + "'";
              connection.query($query2, function (err, del_pro, fields) {
                for (var i = 0; i < req.body.category.length; i++) {
                  $q1 = "INSERT INTO project_categories(project_id,category_id) VALUES('" + project_id + "','" + req.body.category[i]['category_id'] + "')";
                  connection.query($q1, function (err, rows, fields) {
                    if (err) {
                      result = {
                        'status': 'failed',
                        'data': 'An error ocurred while performing the query.'
                      };
                      res.status(500).json(result);
                      return;
                    }
                  });
                } //end for loop

                result = {
                  'status': 'success',
                  'data': 'project updated'
                };
                res.status(200).json(result);

              });
            }


          });

        }
      });
    } else {
      result = {
        'status': 'failed',
        'data': 'login Please'
      };
      res.status(200).json(result);
    }

  });

});

router.post('/project_detail_for_edit', jsonParser, function (req, res) {

  var result = {};
  var project_alias = req.body.project_alias;

  if (typeof project_alias == 'undefined') {
    result = {
      'status': 'failed',
      'data': 'there is no project'
    };
    res.status(200).json(result);
    return;
  }

  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var user_id = rows[0]['user_id'];
      $query2 = "select project_id,project_title,project_alias,project_description,project_budget,project_timeframe from projects where project_alias='" + project_alias + "' and client_id='" + user_id + "'";
      connection.query($query2, function (err, proje, field) {
        if (proje[0]) {
          var project_id = proje[0].project_id;
          $query3 = "select category_id from project_categories where project_id='" + project_id + "'";
          connection.query($query3, function (err, categories, field) {
            if (categories[0]) {
              result = {
                'status': 'success',
                'data': proje[0],
                'categories': categories
              };
              res.status(200).json(result);
            }

          });

        } else {
          result = {
            'status': 'failed',
            'data': 'no project found'
          };
          res.status(200).json(result);
        }

      });
    } else {
      result = {
        'status': 'failed',
        'data': 'invalid user'
      };
      res.status(200).json(result);
    }

  });

});

/*router.post('/project_detail',jsonParser,function (req, res)  {
    var result = {};
    var bid_arr = {};
    var is_apply = false;
    var project_id;
    var categories = "";
    $query1 = "SELECT user_id,user_subscription_status FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
    connection.query($query1, function(err, user_rows, fields) {
      if (err) {
        result = {
          'status': 'failed',
          'data': 'An error ocurred while performing the query.'
        };
        res.status(500).json(result);
        return;
      }
      if (user_rows[0]) {
        var user_id = user_rows[0]['user_id'];
        var user_subscription_status = user_rows[0]['user_subscription_status'];
        $query = "SELECT *,case project_state when '0' then 'Open' when '1' then 'Awarded' when '2' then 'Completed' when '3' then 'Unpublished' when '4' then 'Cancle' end as pstate FROM projects WHERE project_alias='" + req.body.project_alias + "'";

        connection.query($query, function(err, rows, fields) {

          if (rows[0]) {
            project_id = rows[0]['project_id'];
            $query2 = "SELECT * FROM freelancers_projects WHERE project_id='" + project_id + "' AND freelancer_id='" + user_id + "' ";
            //console.log($query2);
            connection.query($query2, function(err, check_rows, fields) {
              if (check_rows[0]) {
                is_apply = true;
                bid_arr = check_rows[0];
              } else {
                is_apply = false;
              }

              $query3 = "SELECT categories.category_name FROM project_categories INNER JOIN categories ON categories.category_id=project_categories.category_id WHERE project_id='" + project_id + "'";
              //console.log($query2);
              connection.query($query3, function(err, check_cate, fields) {
                var arrr = [];
                if (check_cate[0]) {
                  check_cate.forEach(function(entry) {
                    arrr.push(entry.category_name);
                  });
                  categories = arrr.toString(',  ');
                }
                result = {
                  'status': 'success',
                  'data': rows[0],
                  'is_apply': is_apply,
                  'user_subscription_status':user_subscription_status,
                  'categories': categories,
                  'bid_detail': bid_arr
                };
                res.status(200).json(result);

              });


            });

          } else {
            result = {
              'status': 'failed',
              'data': 'project not available'
            };
            res.status(200).json(result);
          }
        });

      } else {
        result = {
          'status': 'failed',
          'data': 'login Please'
        };
        res.status(500).json(result);
      }
    });

});*/
router.post('/project_detail', jsonParser, function (req, res) {
  var result = {};
  var bid_arr = {};
  var is_apply = false;
  var project_id;
  var categories = "";
  $query1 = "SELECT user_id,user_subscription_status FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query1, function (err, user_rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (user_rows[0]) {
      var user_id = user_rows[0]['user_id'];
      var user_subscription_status = user_rows[0]['user_subscription_status'];
      $query = "SELECT *,case project_state when '0' then 'Open' when '1' then 'Awarded' when '2' then 'Completed' when '3' then 'Unpublished' when '4' then 'Cancle' end as pstate FROM projects WHERE project_alias='" + req.body.project_alias + "'";

      connection.query($query, function (err, rows, fields) {

        if (rows[0]) {
          project_id = rows[0]['project_id'];
          $query2 = "SELECT * FROM freelancers_projects WHERE project_id='" + project_id + "' AND freelancer_id='" + user_id + "' ";
          //console.log($query2);
          connection.query($query2, function (err, check_rows, fields) {
            if (check_rows[0]) {
              is_apply = true;
              bid_arr = check_rows[0];
            } else {
              is_apply = false;
            }

            $query3 = "SELECT categories.category_name FROM project_categories INNER JOIN categories ON categories.category_id=project_categories.category_id WHERE project_id='" + project_id + "'";
            //console.log($query2);
            connection.query($query3, function (err, check_cate, fields) {
              var arrr = [];
              if (check_cate[0]) {
                check_cate.forEach(function (entry) {
                  arrr.push(entry.category_name);
                });
                categories = arrr.toString(',  ');
                //get remaining bids


              }

              var rem_bids;
              $query4 = "SELECT *,CONCAT(valid_till, ' 23:59:59') as sub_to FROM subscription_log WHERE user_id='" + user_id + "' ORDER BY sub_log_id DESC LIMIT 1";
              connection.query($query4, function (err, rows1, fields) {
                if (err) {
                  result = {
                    'status': 'failed',
                    'data': 'An error ocurred while performing the query.'
                  };
                  res.status(500).json(result);
                  return;
                }

                if (rows1[0]) {
                  sub_from = rows1[0].added_date.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                  sub_to = rows1[0].sub_to;
                  sub_type = rows1[0].subscription_id;
                } else {
                  sub_from = '';
                  sub_to = '';
                  sub_type = 1;
                }

                if (sub_type != 1) {
                  $query5 = "SELECT count(*) as bid_count FROM freelancers_projects WHERE MONTH(fp_bid_date) = MONTH(CURRENT_DATE()) AND freelancer_id='" + user_id + "'";
                } else {
                  $query5 = "SELECT count(*) as bid_count FROM freelancers_projects WHERE fp_bid_date BETWEEN '" + sub_from + "' AND '" + sub_to + "' AND freelancer_id='" + user_id + "'";
                }

                connection.query($query5, function (err, rows2, fields) {
                  if (rows2[0]) {
                    if (user_subscription_status == 1) {
                      rem_bids = 10 - rows2[0].bid_count;

                    } else {
                      rem_bids = 0;
                    }
                  }

                  result = {
                    'status': 'success',
                    'data': rows[0],
                    'is_apply': is_apply,
                    'user_data': user_rows[0],
                    'categories': categories,
                    'bid_detail': bid_arr,
                    'rem_bids': rem_bids
                  };
                  res.status(200).json(result);

                });
              });


            });


          });

        } else {
          result = {
            'status': 'failed',
            'data': 'project not available'
          };
          res.status(200).json(result);
        }
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'login Please'
      };
      res.status(500).json(result);
    }
  });

});

router.post('/developer_application', jsonParser, function (req, res) {
  var project_data;
  var applications = [];
  var result = {};
  var project_id;

  $query1 = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query1, function (err, user_rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query3.'
      };
      res.status(500).json(result);
      return;
    }
    if (user_rows[0]) {
      $query2 = "select project_id,project_title,project_awarded_to,case project_state when '0' then 'Open' when '1' then 'Awarded' when '2' then 'Completed' when '3' then 'Unpublished' when '4' then 'Cancle' end as status,project_date from projects where project_alias='" + req.body.project_alias + "'";
      connection.query($query2, function (err, rows, fields) {

        if (rows[0]) {
          project_id = rows[0].project_id;
          project_data = rows[0];
          $query3 = "select freelancers_projects.*,users.user_fname,users.user_lname,users.user_city,users.user_username,case users.user_type when '2' then 'Freelancer' when '3' then 'Company' end as usertype, (SELECT COUNT(O.fp_id) FROM freelancers_portfolio as O WHERE O.freelancer_id = freelancers_projects.freelancer_id) as portfolio_count from freelancers_projects LEFT JOIN users ON users.user_id = freelancers_projects.freelancer_id where project_id='" + project_id + "' ORDER BY freelancers_projects.fp_id DESC";
          connection.query($query3, function (err, rows, fields) {
            if (rows[0]) {
              applications = rows;
              result = {
                'status': 'success',
                'project_data': project_data,
                'applications': applications
              };
              res.status(200).json(result);
            } else {
              result = {
                'status': 'success',
                'project_data': project_data,
                'applications': 'No applications found'
              };
              res.status(200).json(result);
            }
          });

        } else {
          result = {
            'status': 'failed',
            'data': 'No project found for this alias'
          };
          res.status(200).json(result);
        }

      });
    } else {
      result = {
        'status': 'failed',
        'data': 'No user found'
      };
      res.status(200).json(result);
    }

  });
});


////////////9-6-17/////
router.post('/check_email', jsonParser, function (req, res) {
  var result = {};
  var email = '';
  if (req.body.email) {
    email = req.body.email;
    $query = "SELECT user_email from users where user_email='" + email + "'";
    connection.query($query, function (err, rows, fields) {
      if (err) {
        result = {
          'status': 'failed',
          'data': 'An error ocurred while performing the query.'
        };
        res.status(500).json(result);
        return;
      }
      if (rows.length > 0) {
        result = {
          'status': 'success',
          'data': 'already exist'
        };
        res.status(200).json(result);
      } else {
        result = {
          'status': 'failed',
          'data': 'not available'
        };
        res.status(200).json(result);
      }
    });
  } else {
    result = {
      'status': 'failed',
      'data': 'email is required'
    };
    res.status(200).json(result);
  }
});


router.post('/registration_part1', jsonParser, function (req, res) {
  var result = {};
  var time = new Date().getTime();
  var first_name = req.body.fname;
  var last_name = req.body.lname;
  var company_name = req.body.company_name;
  var image = req.body.image;
  var email = req.body.email;
  var password = req.body.password;
  var city = req.body.city;
  var country = req.body.country;
  var lat = req.body.lat;
  var lng = req.body.lng;
  var user_type = req.body.user_type;
  var mobile = req.body.mobile;
  var user_auth_token = md5(time) + time;
  var user_name = "";

  if (typeof req.body.mobile != 'undefined' && typeof req.body.fname != 'undefined' && typeof req.body.lname != 'undefined' && typeof req.body.email != 'undefined' && typeof req.body.password != 'undefined' && typeof req.body.city != 'undefined' && typeof req.body.country != 'undefined' && typeof req.body.lat != 'undefined' && typeof req.body.lng != 'undefined' && typeof req.body.user_type != 'undefined') {

    if (user_type == 2) {
      user_name = (first_name + last_name).trim().toLowerCase() + getRandomInteger();
    } else if (user_type == 3) {
      user_name = seoUrl(company_name).toLowerCase() + getRandomInteger();
    }
    $query_ch = "SELECT user_email FROM users WHERE user_email='" + email + "'";
    connection.query($query_ch, function (err, query_email, fields) {
      if (query_email[0]) {
        result = {
          'status': 'already',
          'data': 'email already exist!!'
        };
        res.status(200).json(result);
      } else {

        $query = "INSERT INTO users SET `user_mobile`='" + mobile + "',`user_username`='" + user_name + "',`user_auth_token`='" + user_auth_token + "',`user_fname`='" + first_name + "',`user_lname`='" + last_name + "',`user_company_name`='" + company_name + "',`user_image`='" + image + "',`user_email`='" + email + "',`email_token`='" + md5(time) + "',`user_password`='" + md5(password) + "',`user_city`='" + city + "',`user_country`='" + country + "',`user_lat`='" + lat + "',`user_lng`='" + lng + "',`user_type`='" + user_type + "',`user_reg_status`='1'";
        connection.query($query, function (err, rows, fields) {
          if (err) {
            result = {
              'status': 'failed',
              'data': 'An error ocurred while performing the query.',
              //'query': $query
            };
            res.status(500).json(result);
            return;
          }
          if (rows.affectedRows > 0) {
            if (user_type != 1) {
              $query2 = "INSERT INTO user_state(user_id) VALUES('" + rows.insertId + "')";
              connection.query($query2, function (err, q2, fields) {});
            }
            var link = base_url + "account-verification?email=" + email + "&token=" + md5(time);
            var request = sg.emptyRequest({
              method: 'POST',
              path: '/v3/mail/send',
              body: {
                personalizations: [{
                  to: [{
                    email: email
                  }],
                  subject: 'Please verify your email - EngineerBabu',
                  substitutions: {
                    "{user_fname}": first_name,
                    "{verify_link}": link
                  }
                }],
                from: {
                  email: 'no-reply@engineerbabu.com'
                },
                template_id: 'fcc6c155-b792-4db1-817c-4481abeed712',
              }
            });
            // With promise
            sg.API(request)
              .then(function (response) {
                // console.log(response.statusCode);
              })
              .catch(function (error) {

                //console.log(error.response.statusCode);
              });

            ////////////////////////CODE FOR SLACK/////////////////
            var url = base_url + "profile/" + user_name;
            var usertype = ''
            if (user_type == 1) {
              usertype = 'Client';
            } else if ('user_type' == 2) {
              usertype = 'Freelancer';
            } else {
              usertype = 'Company';
            }

            var name = first_name + " " + last_name;

            var options = {
              "method": "POST",
              "hostname": "sokt.io",
              "port": null,
              "path": "/KXVBPGyx2GCpEmxE2UVu/eb-newuser?name=" + urlencode(name) + "&url=" + urlencode(url) + "&usertype=" + urlencode(usertype) + "&city=" + urlencode(city) + "",
              "headers": {
                "authkey": "ujA3BSHsSh66Qjd2tjaM",
                "content-type": "application/json"
              }
            };
            var req = http.request(options, function (res) {
              var chunks = [];

              res.on("data", function (chunk) {
                chunks.push(chunk);
              });

              res.on("end", function () {
                var body = Buffer.concat(chunks);
                // console.log(body.toString());
              });
            });

            // req.write(JSON.stringify({ hello: 'world' }));
            req.end();

            ////////////////////////CODE END/////////////////////          


            result = {
              'status': 'success',
              'token': user_auth_token,
              'user_name': user_name,
              'user_fname': first_name,
              'reg_type': user_type,
              'user_reg': 1
            };
            res.status(200).json(result);
          } else {
            result = {
              'status': 'failed',
              'data': 'Something went wrong'
            };
            res.status(200).json(result);
          }
        });
      }
    });
  } else {
    result = {
      'status': 'failed',
      'data': 'Invalid Request parameter'
    };
    res.status(200).json(result);
  }
});



router.post('/registration_part2', jsonParser, function (req, res) {
  var result = {};

  var about = req.body.about.replace(/'/g, "");
  var user_url = req.body.web_url;
  var facebook = req.body.facebook_url;
  var twitter = req.body.twitter_url;
  var linkedin = req.body.linkdin_url;
  var token = req.headers.secret_key;
  var user_image = req.body.image;
  var image_handler = req.body.image_handler;
  var time = new Date().getTime();

  if (typeof token == 'undefined' || token == '' || token == null) {
    result = {
      'status': 'failed',
      'data': "unauthorised access"
    };
    res.status(200).json(result);
    return;
  }
  $query = "SELECT user_id,user_fname,user_email FROM users WHERE user_auth_token='" + token + "'";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.',
        'query': $query
      };
      res.status(500).json(result);
    }
    if (rows[0]) {
      var user_id = rows[0]['user_id'];
      var user_fname = rows[0]['user_fname'];
      var user_email = rows[0]['user_email'];
      var user_type = rows[0]['user_type'];


      $query1 = "UPDATE users SET `user_about`='" + about + "',`user_image`='" + user_image + "',`image_handler`='" + image_handler + "',`user_url`='" + user_url + "',`user_fb_url`='" + facebook + "',`user_twitter_url`='" + twitter + "',`user_linkdin_url`='" + linkedin + "',`user_reg_status`=2 WHERE user_id ='" + user_id + "' ";

      connection.query($query1, function (err1, rows1, fields) {
        if (err1) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.',
            'query': $query1
          };
          res.status(500).json(result);
          return;
        }
        if (rows1.affectedRows > 0) {

          $data = {
            "token": token,
            "user_id": user_id
          }

          var request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: {
              personalizations: [{
                to: [{
                  email: user_email
                }],
                subject: 'Welcome to Engineerbabu, It\'s your friend Mayank',
                substitutions: {
                  "{user_fname}": user_fname,
                }
              }],
              from: {
                email: 'no-reply@engineerbabu.com'
              },
              template_id: '0ccf02e6-c13f-414f-b91e-c7ec52bcf3af',
            }
          });
          // With promise
          sg.API(request)
            .then(function (response) {
              // console.log(response.statusCode);
            })
            .catch(function (error) {

              console.log(error.response.statusCode);
            });

          result = {
            'status': 'success',
            'data': $data,
            'user_reg': 2
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'something went wrong plese try again letter'
          };
          res.status(200).json(result);
        }
      });
    } else {
      result = {
        'status': 'failed',
        'data': 'Invalid token'
      };
      res.status(200).json(result);
    }
  });
});


router.get('/get_freelancer_dashboard', jsonParser, function (req, res) {
  var result = {};
  var profile_count;
  var portfolio_count;
  var user_hash;


  if (typeof req.headers.secret_key === 'undefined' || req.headers.secret_key === null || req.headers.secret_key === '') {
    result = {
      'status': 'failed',
      'data': 'unauthorised access'
    };
    res.status(200).json(result);
    return;
    // return;
  } else {
    var secret_key = req.headers.secret_key;
  }

  $query1 = "SELECT users.*,user_state.* FROM users LEFT JOIN user_state ON users.user_id=user_state.user_id WHERE user_auth_token='" + secret_key + "'  LIMIT 1";
  connection.query($query1, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var user_id = rows[0].user_id;
      var user_email = rows[0].user_email;

      var user_subscription_status = rows[0].user_subscription_status;
      var user_data = rows[0];
      var sub_type;
      var active_chat_count;

      $query5 = "SELECT count(*) as chat_count FROM message_thread WHERE freelancer_id='" + user_id + "'";
      connection.query($query5, function (err, rows2, fields) {
        if (rows2[0]) {
          active_chat_count = rows2[0].chat_count;
        }

        //intercom hash
        user_hash = crypto.createHmac("sha256", 'enwyCS6BI1LktwZ7uI9Psu-_632ZiSrs1rfDD-hQ').update(user_email).digest("hex");

        result = {
          'status': 'success',
          'data': user_data,
          'user_hash': user_hash,
          'profile_count': user_data.profile_view_count,
          'rem_bids': user_data.remaining_bid,
          'active_chat_count': active_chat_count,
          'portfolio_count': user_data.portfolio_count
        };
        res.status(200).json(result);

      });





    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }

  });

});

router.post('/account_verification', jsonParser, function (req, res) {
  var result = {};
  var email = '';
  var token = '';

  if (req.body.email && req.body.token) {
    email = req.body.email;
    token = req.body.token;
  }

  $query = "SELECT user_id,user_type,user_email_confirmed from users where user_email='" + email + "' and email_token='" + token + "'";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows.length > 0) {

      var user_id = rows[0]['user_id'];
      var user_type = rows[0]['user_type'];

      if (rows[0]['user_email_confirmed'] == 0) {

        $query2 = "UPDATE users SET user_email_confirmed='1',email_token='' WHERE user_id ='" + user_id + "'";
        connection.query($query2, function (err, rows, fields) {
          if (rows.affectedRows > 0) {
            result = {
              'status': 'success',
              'data': user_type
            };
            res.status(200).json(result);
          } else {
            result = {
              'status': 'failed',
              'data': 'Opps something went wrong. Please try again.'
            };
            res.status(200).json(result);
          }

        });

      } else {
        result = {
          'status': 'already'
        };
        res.status(200).json(result);
      }
    } else {
      result = {
        'status': 'failed',
        'data': 'Account verification failed!! Token mismatch'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.post('/login', jsonParser, function (req, res) {
  var result = {};


  var device_token = '';
  var device_id = '';
  var device_type = '';

  if (typeof req.body.device_token != 'undefined' && req.body.device_token) {
    device_token = req.body.device_token;
  }

  if (typeof req.body.device_id != 'undefined' && req.body.device_id) {
    device_id = req.body.device_id;
  }

  if (typeof req.body.device_type != 'undefined' && req.body.device_type) {
    device_type = req.body.device_type;
  }

  $query = "SELECT * FROM users WHERE user_email='" + req.body.email + "' AND user_password='" + md5(req.body.password) + "' LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows[0]) {
      user_id = rows[0]['user_id'];
      //if (rows[0]['user_email_confirmed'] == 1) {
      $query2 = "UPDATE users SET device_token = '" + device_token + "',device_id = '" + device_id + "',device_type = '" + device_type + "' WHERE user_id='" + user_id + "'";
      connection.query($query2, function (err1, rows1, fields) {
        if (err1) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          //console.log("An error ocurred performing the query.");
          return;
        }
        //console.log
        if (rows1.affectedRows > 0) {
          result = {
            'status': 'success',
            'data': rows[0]
          };
          res.status(200).json(result);
        }
      });

      /*} else {
        result = {
          'status': 'failed',
          'data': 'Email not confirmed please check your email'
        };
        res.status(200).json(result);
      }*/
    } else {
      result = {
        'status': 'failed',
        'data': 'Email or password is incorrect'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});
////////////9-6-17 end/////



router.post('/add_team', jsonParser, function (req, res) {
  var result = {};
  var user_id;
  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $q1 = "INSERT INTO team(user_id,f_name,l_name,profile_pic,designation) VALUES('" + user_id + "','" + req.body.f_name + "','" + req.body.l_name + "','" + req.body.profile_pic + "','" + designation + "')";
      connection.query($q1, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }

        result = {
          'status': 'success',
          'data': rows
        };
        res.status(200).json(result);

      });


    } else {
      result = {
        'status': 'failed',
        'data': 'Skill not available'
      };
      res.status(200).json(result);
    }


  });

});

router.post('/edit_team', jsonParser, function (req, res) {
  var result = {};

  var user_id;
  $query1 = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query1, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "update team SET f_name = '" + req.body.f_name + "',l_name = '" + req.body.l_name + "',profile_pic = '" + req.body.profile_pic + "',designation = '" + req.body.designation + "'";
      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows.affectedRows > 0) {
          result = {
            'status': 'success'
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'not updated'
          };
          res.status(200).json(result);
        }
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }




  });
});

router.post('/delete_team', jsonParser, function (req, res) {
  var result = {};

  var user_id;
  $query1 = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query1, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "delete from team where id='" + req.body.team_id + "' and user_id='" + user_id + "'";
      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows.affectedRows > 0) {
          result = {
            'status': 'success'
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'delete Failed'
          };
          res.status(200).json(result);
        }
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }




  });
});



///social signup api//
router.post('/social_signup', jsonParser, function (req, res) {
  var result = {};
  var device_token = '';
  var device_id = '';
  var device_type = '';
  var name = req.body.name.split(' ');
  var fname = '';
  var lname = '';
  var time = new Date().getTime();
  var image = req.body.image;
  var email = req.body.email;



  if (req.body.device_token) {
    device_token = req.body.device_token;
  }

  if (req.body.device_id) {
    device_id = req.body.device_id;
  }

  if (req.body.device_type) {
    device_type = req.body.device_type;
  }

  if (name.length == 2) {
    fname = name[0];
    lname = name[1];
  } else {
    fname = name[0];
  }

  var social_data = JSON.stringify(req.body);

  $query = "SELECT * FROM users WHERE user_email='" + req.body.email + "' LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows.length > 0) {
      user_id = rows[0]['user_id'];
      if (rows[0]['user_email_confirmed'] == 1) {
        $query2 = "UPDATE users SET device_token = '" + device_token + "',device_id = '" + device_id + "',device_type = '" + device_type + "' WHERE user_id='" + user_id + "'";
        connection.query($query2, function (err1, rows1, fields) {
          if (err1) {
            result = {
              'status': 'failed',
              'data': 'An error ocurred while performing the query.'
            };
            res.status(500).json(result);
            //console.log("An error ocurred performing the query.");
            return;
          }
          //console.log
          if (rows1.affectedRows > 0) {
            result = {
              'status': 'success',
              'data': rows[0]
            };
            res.status(200).json(result);
          }
        });

      } else {
        result = {
          'status': 'failed',
          'data': 'Email not confirmed please check your email'
        };
        res.status(200).json(result);
      }
    } else {
      //create user with basic info. confirm email_id and creta auth_token;
      $query = "INSERT INTO users SET `user_auth_token`='" + md5(time) + time + "',`user_fname`='" + fname + "',`user_lname`='" + lname + "',`user_image`='" + image + "',`user_email`='" + email + "',`user_reg_status`='1',`user_status`='1',`user_email_confirmed`='1',`social_data`='" + social_data + "'";
      connection.query($query, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'

          };
          res.status(500).json(result);
          //console.log("An error ocurred performing the query.");
          return;
        }
        if (rows.affectedRows > 0) {
          result = {
            'status': 'success',
            'data': md5(time) + time
          };
          res.status(200).json(result);
        }

      });
    }
  });

});
//end social signup api//


//payment_apis//
router.post('/one_month_payment', jsonParser, function (req, res) {

  var result = {};
  var payment_id;
  var amount = 236000;
  var user_profile_completed;
  var user_info;
  var name = '';
  var user_profile = '';

  if (req.body.razorpay_payment_id) {

    var user_id;
    $query = "SELECT user_id,user_reg_status,user_email_confirmed,user_image,user_fname,user_lname,user_username,user_email FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
    connection.query($query, function (err, user_infos, fields) {
      if (err) {
        result = {
          'status': 'failed',
          'data': 'An error ocurred while performing the query.'
        };
        res.status(500).json(result);
        return;
      }
      if (user_infos[0]) {

        user_id = user_infos[0]['user_id'];
        user_info = user_infos[0];
        name = user_infos[0]['user_fname'] + " " + user_infos[0]['user_lname'];
        user_name = user_infos[0]['user_username'];
      }




      payment_id = req.body.razorpay_payment_id;
      //Capture a particular payment
      instance.payments.capture(payment_id, amount).then((data) => {

        var razorpay_data = JSON.stringify(data);

        $q1 = "INSERT INTO subscription_log(subscription_id,user_id,status,valid_till,payment_id,payment_info) VALUES(1,'" + user_id + "',1,NOW()+INTERVAL 30 DAY,'" + payment_id + "','" + razorpay_data + "')";
        connection.query($q1, function (err, rows1, fields) {
          if (err) {

            result = {
              'status': 'failed',
              'data': 'An error ocurred while performing the query.',
              'query': $q1
            };
            res.status(500).json(result);
            //console.log("An error ocurred performing the query.");
            return;
          }

          if (rows1.affectedRows > 0) {

            user_profile_completed = calculate_profile_percent(user_info);

            $q2 = "update users SET user_subscription_type = 1,user_subscription_valid_till = NOW()+INTERVAL 30 DAY,user_subscription_status = 1,user_profile_completed = '" + user_profile_completed + "' WHERE user_id='" + user_id + "'";
            connection.query($q2, function (err, rows2, fields) {
              if (err) {
                result = {
                  'status': 'failed',
                  'data': 'An error ocurred while performing the query.'
                };
                res.status(500).json(result);
                return;
              }
              if (rows2.affectedRows > 0) {

                $q6 = "UPDATE user_state SET remaining_bid=10 where user_id='" + user_id + "'";
                connection.query($q6, function (err, q6, fields) {});

                var notifications_data = JSON.stringify({
                  "notify_type": "normal",
                  "image_icon": "assets/img/eb_logo_38x38.jpg",
                  "message": "Congratulations for being a paid partner, your subscription is active start bidding today.",
                  "view_btn": 0,
                  "btn_href": ""
                });

                $q7 = "INSERT INTO notifications(user_id,data) VALUES ('" + user_id + "','" + notifications_data + "')";
                connection.query($q7, function (err, rows7, fields) {});


                var request = sg.emptyRequest({
                  method: 'POST',
                  path: '/v3/mail/send',
                  body: {
                    personalizations: [{
                      to: [{
                        email: user_info['user_email']
                      }],
                      subject: 'EngineerBabu: Congratulations!! Subscription Successful',
                      substitutions: {
                        "{user_fname}": user_info['user_fname'],
                      }
                    }],
                    from: {
                      email: 'no-reply@engineerbabu.com'
                    },
                    template_id: 'dd8f0d3e-843d-4840-97ab-88e67e802ba0',
                  }
                });
                // With promise
                sg.API(request)
                  .then(function (response) {
                    // console.log(response.statusCode);
                  })
                  .catch(function (error) {

                    // console.log(error.response.statusCode);
                  });

                $q3 = "SELECT * FROM users WHERE user_id='" + user_id + "' LIMIT 1";
                connection.query($q3, function (err, rows3, fields) {
                  if (err) {
                    result = {
                      'status': 'failed',
                      'data': 'An error ocurred while performing the query.'
                    };
                    res.status(500).json(result);
                    return;
                  }

                  if (rows3[0]) {

                    $q4 = "SELECT * FROM subscription_log WHERE user_id='" + user_id + "' ORDER by sub_log_id";
                    connection.query($q4, function (err, trans_rows, fields) {

                      ////////////////////////CODE FOR SLACK/////////////////
                      var url = base_url + "profile/" + user_name;

                      var options = {
                        "method": "POST",
                        "hostname": "sokt.io",
                        "port": null,
                        "path": "/2hbuFhCkmsHcm8wq2X18/eb-paid?name=" + urlencode(name) + "&url=" + urlencode(url),
                        "headers": {
                          "authkey": "ujA3BSHsSh66Qjd2tjaM",
                          "content-type": "application/json"
                        }
                      };
                      var req = http.request(options, function (res) {
                        var chunks = [];

                        res.on("data", function (chunk) {
                          chunks.push(chunk);
                        });

                        res.on("end", function () {
                          var body = Buffer.concat(chunks);
                          // console.log(body.toString());
                        });
                      });

                      // req.write(JSON.stringify({ hello: 'world' }));
                      req.end();
                      ////////////////////////CODE END FOR SLACK/////////////////

                      result = {
                        'status': 'success',
                        'data': trans_rows,
                        'user_data': rows3[0]

                      };
                      res.status(200).json(result);

                    });
                  }

                });




              } else {
                result = {
                  'status': 'failed',
                  'data': 'not updated'
                };
                res.status(200).json(result);
              }
            });



          } else {
            result = {
              'status': 'failed',
              'data': 'problem in add'
            };
            res.status(200).json(result);
          }

        });
      }).catch((error) => {

        result = {
          'status': 'failed',
          'data': 'An error ocurred',
          'error': error
        };
        res.status(500).json(result);
      });



    });
  } else {
    result = {
      'status': 'error',
      'data': 'payment id not found'
    };
    res.status(500).json(result);
  }
});
//end payment apis//



router.post('/check_password_auth', jsonParser, function (req, res) {
  var result = {};
  var email = '';
  var token = '';
  if (req.body.email && req.body.token) {
    email = req.body.email;
    token = req.body.token;
  }

  $query = "SELECT user_id from users where user_email='" + email + "' and email_token='" + token + "'";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows.length > 0) {
      result = {
        'status': 'success',
        'data': ''
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'Token mismatch'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.post('/check_reset_password_auth', jsonParser, function (req, res) {
  var result = {};
  var email = '';
  var token = '';
  if (req.body.email && req.body.token) {
    email = req.body.email;
    token = req.body.token;
  }

  $query = "SELECT user_id from users where user_email='" + email + "' and user_password_reset_code='" + token + "'";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows.length > 0) {
      result = {
        'status': 'success',
        'data': ''
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'Token mismatch'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.post('/create_password', jsonParser, function (req, res) {
  var result = {};
  var email = '';
  var token = '';
  var password = '';
  if (req.body.email && req.body.token) {
    email = req.body.email;
    token = req.body.token;
    password = req.body.conf_password;
  }

  $query = "SELECT user_id from users where user_email='" + email + "' and email_token='" + token + "'";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows.length > 0) {
      var user_id = rows[0]['user_id'];
      $query2 = "UPDATE users SET user_password='" + md5(password) + "',user_email_confirmed='1',email_token='' WHERE user_id ='" + user_id + "'";
      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows.affectedRows > 0) {
          result = {
            'status': 'success',
            'data': 'Password changed succesfully'
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'Create Password failed'
          };
          res.status(200).json(result);
        }

      });
    } else {
      result = {
        'status': 'failed',
        'data': 'Token mismatch'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.post('/create_reset_password', jsonParser, function (req, res) {
  var result = {};
  var email = '';
  var token = '';
  var password = '';
  if (req.body.email && req.body.token) {
    email = req.body.email;
    token = req.body.token;
    password = req.body.conf_password;
  }

  $query = "SELECT user_id from users where user_email='" + email + "' and user_password_reset_code='" + token + "'";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows.length > 0) {
      var user_id = rows[0]['user_id'];
      $query2 = "UPDATE users SET user_password='" + md5(password) + "',user_email_confirmed='1',user_password_reset_code='' WHERE user_id ='" + user_id + "'";
      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows.affectedRows > 0) {
          result = {
            'status': 'success',
            'data': 'Password changed succesfully'
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'Create Password failed'
          };
          res.status(200).json(result);
        }

      });
    } else {
      result = {
        'status': 'failed',
        'data': 'Token mismatch'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

//common-apis///
router.get('/getall_countries', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT iso,name FROM countries";

  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'countries not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});


router.get('/get_category', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT * FROM categories";

  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'categories not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.get('/get_portfolio_by_id', jsonParser, function (req, res) {
  var result = {};

  $query = "SELECT * FROM freelancers_portfolio WHERE fp_id='" + req.query.pf_id + "'";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows[0]
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'Portfolio not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.get('/get_skills_by_name', jsonParser, function (req, res) {
  var result = {};
  var search_key = req.query.search_key;

  $query = "SELECT skill_id,skill_name FROM skills WHERE skill_name like '%" + search_key + "%' LIMIT 6";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'Skill not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.get('/get_profile_count', jsonParser, function (req, res) {
  var result = {};
  var user_id = '';
  var profile_count = '';

  $query1 = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "'  LIMIT 1";
  connection.query($query1, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "SELECT count(project_id) AS project_count FROM projects WHERE client_id='" + user_id + "'";
      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows[0]) {
          profile_count = rows[0]['project_count'];
        } else {
          profile_count = 0;
        }

        result = {
          'status': 'success',
          'data': profile_count
        };
        res.status(200).json(result);


      });
    } else {
      result = {
        'status': 'failed',
        'data': 'user not found'
      };
      res.status(200).json(result);
    }

  });

});
router.post('/get_client_dashboard', jsonParser, function (req, res) {
  var result = {};
  var user_id = '';
  var user_data = [];
  var client_projects = [];
  var user_hash;
  $query1 = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "'  LIMIT 1";
  connection.query($query1, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      if (rows[0]['user_type'] == 1) {
        user_data = rows[0];
        user_id = rows[0]['user_id'];
        $query4 = "select (select count(project_id) as total_project from projects where client_id='" + user_id + "') as total_project,count(*),(select COUNT(*) from users where user_type=2) as developer_count,(select COUNT(*) from users where user_type=3) as company_count,(select COUNT(*) from deals) as deals_count from users";
        connection.query($query4, function (err, counts2, fields) {
          if (counts2[0]) {
            $query2 = "SELECT P.*,CONCAT(LEFT(P.project_description,147),'..') as project_desc,case P.project_state when '0' then 'Open' when '1' then 'Awarded' when '2' then 'Completed' when '3' then 'Unpublished' when '4' then 'Cancle' end as status,(SELECT COUNT(O.project_id) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as count_f,(SELECT MAX(O.fp_bid_amount) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as max_f,(SELECT MIN(O.fp_bid_amount) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as min_f FROM projects as P WHERE P.client_id ='" + user_id + "' ORDER BY P.project_id DESC LIMIT 10";
            connection.query($query2, function (err, rows, fields) {
              if (rows[0]) {
                user_hash = crypto.createHmac("sha256", 'enwyCS6BI1LktwZ7uI9Psu-_632ZiSrs1rfDD-hQ').update(user_data.user_email).digest("hex");

                result = {
                  'user_hash': user_hash,
                  'status': 'success',
                  'data': user_data,
                  'client_projects': rows,
                  'count2': counts2[0]
                };
                res.status(200).json(result);
              } else {

                result = {
                  'status': 'success',
                  'data': user_data,
                  'client_projects': [],
                  'count2': counts2[0]
                };
                res.status(200).json(result);
              }
            });
          }
        })
      } else {
        result = {
          'status': 'failed',
          'data': 'user type not valid'
        };
        res.status(200).json(result);
      }
    } else {
      result = {
        'status': 'failed',
        'data': 'user not found'
      };
      res.status(200).json(result);
    }
  });

});

// router.post('/get_client_dashboard',jsonParser,function (req, res)  {
//   var result = {};
//   var user_id = '';
//   var user_data = [];
//   var client_projects = [];
//   $query1 = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "'  LIMIT 1";
//   connection.query($query1, function(err, rows, fields) {
//     if (err) {

//       result = {
//         'status': 'failed',
//         'data': 'An error ocurred while performing the query.'
//       };
//       res.status(500).json(result);
//       return;
//     }
//     if (rows[0]) {
//       if (rows[0]['user_type'] == 1) {
//         //client
//         user_data = rows[0];
//         user_id = rows[0]['user_id'];

//         $query2 = "SELECT P.*,CONCAT(LEFT(P.project_description,147),'..') as project_desc,case P.project_state when '0' then 'Open' when '1' then 'Awarded' when '2' then 'Completed' when '3' then 'Unpublished' when '4' then 'Cancle' end as status,(SELECT COUNT(O.project_id) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as count_f,(SELECT MAX(O.fp_bid_amount) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as max_f,(SELECT MIN(O.fp_bid_amount) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as min_f FROM projects as P WHERE P.client_id ='" + user_id + "' ORDER BY P.project_id DESC LIMIT 10";
//         connection.query($query2, function(err, rows, fields) {
//           if (err) {
//             result = {
//               'status': 'failed',
//               'data': 'An error ocurred while performing the query.'
//             };
//             res.status(500).json(result);
//             return;
//           }

//           if (rows[0]) {
//             $query3 = "select sum(project_count) as project_view,count(project_id) as total_project from projects where client_id='" + user_id + "'";
//             connection.query($query3, function(err, counts, fields) {
//               if (counts[0]) {
//                 $query4 = "SELECT count(*) AS applied_count FROM `freelancers_projects` LEFT JOIN projects ON projects.project_id=freelancers_projects.project_id LEFT JOIN users ON users.user_id=projects.client_id WHERE users.user_id='" + user_id + "'";
//                 connection.query($query4, function(err, applied, fields) {
//                   if (applied[0]) {
//                     result = {
//                       'status': 'success',
//                       'data': user_data,
//                       'client_projects': rows,
//                       'count': counts[0],
//                       'applied_count': applied[0].applied_count
//                     };
//                     res.status(200).json(result);
//                   }
//                 });

//               }

//             });

//           } else {
//             result = {
//               'status': 'success',
//               'data': user_data,
//               'client_projects': 0,
//               'count': {
//                 "project_view": 0,
//                 "total_project": 0
//               },
//               'applied_count': 0
//             };
//             res.status(200).json(result);
//           }

//         });

//       } else {
//         result = {
//           'status': 'failed',
//           'data': 'user type not valid'
//         };
//         res.status(200).json(result);
//       }
//     } else {


//       result = {
//         'status': 'failed',
//         'data': 'user not found'
//       };
//       res.status(200).json(result);
//     }

//   });

// });

router.post('/load_more_project_for_client', jsonParser, function (req, res) {
  var result = {};
  var user_id = '';
  var client_projects = [];
  $query1 = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "'  LIMIT 1";
  connection.query($query1, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query1.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "SELECT P.*,CONCAT(LEFT(P.project_description,147),'..') as project_desc,case P.project_state when '0' then 'Open' when '1' then 'Awarded' when '2' then 'Completed' when '3' then 'Unpublished' when '4' then 'Cancle' end as status,(SELECT COUNT(O.project_id) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as count_f,(SELECT MAX(O.fp_bid_amount) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as max_f,(SELECT MIN(O.fp_bid_amount) FROM freelancers_projects as O WHERE O.project_id = P.project_id) as min_f FROM projects as P WHERE P.client_id ='" + user_id + "' ORDER BY P.project_id DESC LIMIT " + req.body.offset + ",10";
      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query2'
          };
          res.status(500).json(result);
          return;
        }
        if (rows[0]) {
          result = {
            'status': 'success',
            'client_projects': rows
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'client_projects': 'projects not available'
          };
          res.status(200).json(result);
        }
      });


    } else {
      result = {
        'status': 'failed',
        'data': 'user not found'
      };
      res.status(200).json(result);
    }
  });

});

///end common/////

router.post('/forget_password', jsonParser, function (req, res) {
  var result = {};
  var email = req.body.email;

  var time = new Date().getTime();

  $query = "SELECT user_email,user_fname,user_id FROM users WHERE user_email='" + email + "'";
  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows[0]) {

      var user_id = rows[0]['user_id'];
      var link = base_url + "reset-password?email=" + email + "&token=" + md5(time);

      $query2 = "UPDATE users SET user_password_reset_code = '" + md5(time) + "' WHERE user_id='" + user_id + "'";
      connection.query($query2, function (err1, rows1, fields) {

        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }

        if (rows1.affectedRows > 0) {
          var request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: {
              personalizations: [{
                to: [{
                  email: email
                }],
                subject: 'EngineerBabu: Forgot Password',
                substitutions: {
                  "{user_fname}": rows[0]['user_fname'],
                  "{verify_link}": link
                }
              }],
              from: {
                email: 'no-reply@engineerbabu.com'
              },
              template_id: 'e7f3dc1e-1f34-41ea-9718-95bf426b122a',
            }
          });
          // With promise
          sg.API(request)
            .then(function (response) {
              // console.log(response.statusCode);
            })
            .catch(function (error) {

              console.log(error.response.statusCode);
            });

          result = {
            'status': 'success',
            'data': rows
          };
          res.status(200).json(result);
        }

      });

    } else {
      result = {
        'status': 'failed',
        'data': 'This email is not registered'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.post('/add_portfolio', jsonParser, function (req, res) {
  var result = {};
  var user_id;
  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $q1 = "INSERT INTO freelancers_portfolio(fp_title,fp_image,image_handler,fp_url,category_id,freelancer_id,fp_desc) VALUES('" + req.body.title.replace(/'/g, "") + "','" + req.body.portfolioimage + "','" + req.body.image_handler + "','" + req.body.url + "','" + req.body.category + "','" + user_id + "','" + req.body.description.replace(/'/g, "") + "')";
      connection.query($q1, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }

        $q2 = "UPDATE user_state SET portfolio_count=portfolio_count+1 where user_id='" + user_id + "'";
        connection.query($q2, function (err, row, fields) {

        });


        result = {
          'status': 'success',
          'data': rows
        };
        res.status(200).json(result);

      });


    } else {
      result = {
        'status': 'failed',
        'data': 'User not available'
      };
      res.status(200).json(result);
    }


  });

});

router.post('/edit_portfolio', jsonParser, function (req, res) {
  var result = {};

  var user_id;
  $query1 = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query1, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "update freelancers_portfolio SET fp_title = '" + req.body.title.replace(/'/g, "") + "',fp_image = '" + req.body.portfolioimage + "',image_handler = '" + req.body.image_handler + "',fp_url = '" + req.body.url + "',category_id = '" + req.body.category + "',freelancer_id = '" + user_id + "',fp_desc = '" + req.body.description.replace(/'/g, "") + "' WHERE fp_id='" + req.body.fp_id + "'";
      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows.affectedRows > 0) {
          result = {
            'status': 'success'
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'not updated'
          };
          res.status(200).json(result);
        }
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }




  });
});


router.post('/delete_portfolio', jsonParser, function (req, res) {
  var result = {};
  var portfolio = [];
  var user_id;

  if (typeof req.body.portfolio_id == 'undefined' || req.body.portfolio_id == null || req.body.portfolio_id == '') {
    result = {
      'status': 'failed'

    };
    res.status(200).json(result);
    return;
  }

  $query1 = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query1, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query1.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "delete from freelancers_portfolio where fp_id='" + req.body.portfolio_id + "'";

      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query2.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows.affectedRows > 0) {
          $q3 = "UPDATE user_state SET portfolio_count=portfolio_count-1 where user_id='" + user_id + "'";
          connection.query($q3, function (err, row, fields) {});
          result = {
            'status': 'success',
            'data': 'Portfolio deleted successfully.'
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'delete Failed'
          };
          res.status(200).json(result);
        }
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }




  });
});

router.post('/get_userbyid', jsonParser, function (req, res) {
  var result = {};
  var user_id;
  var category = [];
  var skills = [];

  $query = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "'  LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "SELECT freelancers_categories.fc_id,categories.* FROM `freelancers_categories` LEFT JOIN categories on freelancers_categories.category_id=categories.category_id WHERE freelancers_categories.freelancer_id='" + user_id + "'";

      connection.query($query2, function (err, categories, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query2.'
          };
          res.status(500).json(result);
          return;
        }
        if (categories[0]) {
          category = categories;

        }

        $query3 = "SELECT freelancers_skills.fskill_id,freelancers_skills.skill_id,skills.skill_name FROM `freelancers_skills` LEFT JOIN skills on freelancers_skills.skill_id=skills.skill_id WHERE freelancers_skills.freelancer_id='" + user_id + "'";
        connection.query($query3, function (err, skills_row, fields) {
          if (err) {
            result = {
              'status': 'failed',
              'data': 'An error ocurred while performing the query3.'
            };
            res.status(500).json(result);
            return;
          }

          if (skills_row[0]) {
            skills = skills_row;

          }
          result = {
            'status': 'success',
            'data': rows[0],
            'category': category,
            'skills': skills
          };
          res.status(200).json(result);

        });




      });


    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});


router.post('/client_profile_edit', jsonParser, function (req, res) {
  var result = {};

  $query = "update users SET user_fname = '" + req.body.first_name + "',user_lname = '" + req.body.last_name + "',user_city = '" + req.body.city + "',user_country = '" + req.body.country + "',user_about = '" + req.body.about.replace(/'/g, "") + "',user_designation = '" + req.body.designation + "',user_privacy = '" + req.body.privacy + "',user_mobile = '" + req.body.mobile_number + "',user_url = '" + req.body.website_url + "',user_fb_url = '" + req.body.facebook + "',user_linkdin_url = '" + req.body.linkedin + "',user_twitter_url = '" + req.body.twitter + "',user_image='" + req.body.user_image + "',image_handler='" + req.body.image_handler + "' WHERE user_auth_token='" + req.headers.secret_key + "'";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows.affectedRows > 0) {
      //get new data again
      $query2 = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
      connection.query($query2, function (err, user_data, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (user_data[0]) {
          result = {
            'status': 'success',
            'data': user_data[0]
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'user not available'
          };
          res.status(200).json(result);
        }
      });
    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }

  });

});

router.post('/update_userprofile', jsonParser, function (req, res) {
  var result = {};

  $query = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "'  LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }

    if (rows[0]) {
      $query = "update users SET user_fname = '" + req.body.first_name + "',user_lname = '" + req.body.last_name + "',user_city = '" + req.body.city + "',user_country = '" + req.body.country + "',user_about = '" + req.body.about.replace(/'/g, "") + "',user_designation = '" + req.body.designation + "',user_mobile = '" + req.body.mobile_number + "',user_url = '" + req.body.website_url + "',user_fb_url = '" + req.body.facebook + "',user_linkdin_url = '" + req.body.linkedin + "',user_twitter_url = '" + req.body.twitter + "',user_image='" + req.body.profile_image + "',image_handler='" + req.body.image_handler + "',hourly_rate='" + req.body.hourly_rate + "',user_lat='" + req.body.lat + "',user_lng='" + req.body.lng + "' WHERE user_auth_token='" + req.headers.secret_key + "'";

      connection.query($query, function (err, rows, fields) {
        if (rows.affectedRows > 0) {
          //get new data again
          $query2 = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
          connection.query($query2, function (err, user_data, fields) {

            if (user_data[0]) {
              result = {
                'status': 'success',
                'data': user_data[0]
              };
              res.status(200).json(result);
            } else {
              result = {
                'status': 'failed',
                'data': 'user not available'
              };
              res.status(200).json(result);
            }

          });

        } else {
          result = {
            'status': 'failed',
            'data': 'update failed'
          };
          res.status(200).json(result);
        }

      });


    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }

  });

});

router.post('/update_userprofileimage', jsonParser, function (req, res) {
  var result = {};

  $query = "update users SET user_image = '" + req.body.profile_image + "' WHERE user_auth_token='" + req.headers.secret_key + "'";

  connection.query($query, function (err, rows, fields) {

    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows.affectedRows > 0) {
      //get new data again
      $query2 = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
      connection.query($query2, function (err, user_data, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (user_data[0]) {
          result = {
            'status': 'success',
            'data': user_data[0]
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'user not available'
          };
          res.status(200).json(result);
        }

      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available'
      };
      res.status(200).json(result);
    }

  });
});

router.post('/update_userpassword', jsonParser, function (req, res) {
  var result = {};
  $query1 = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "' AND user_password = '" + md5(req.body.current_password) + "'";
  connection.query($query1, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      $query2 = "update users SET user_password = '" + md5(req.body.conf_password) + "' WHERE user_auth_token='" + req.headers.secret_key + "'";
      connection.query($query2, function (err, user_data, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (user_data.affectedRows > 0) {
          result = {
            'status': 'success'
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed'
          };
          res.status(200).json(result);
        }

      });


    } else {
      result = {
        'status': 'failed',
        'data': 'invalid current password'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});


router.post('/quick_post_project', jsonParser, function (req, res) {
  var result = {};
  var user_id;
  var user_type;
  var category = req.body.category;
  var platform_list = [];
  var project_title = req.body.project_title.replace(/'/g, "");
  var project_description = req.body.description.replace(/'/g, "");
  var project_alias;
  var ip = req.connection.remoteAddress;
  var name = '';
  var email = '';
  var mobile = '';
  var user_country = '';
  var user_city = '';

  //check user login or not
  if (req.headers.secret_key) {

    $query1 = "SELECT users.*,countries.nicename as country_name FROM users LEFT JOIN countries ON users.user_country = countries.iso WHERE user_auth_token='" + req.headers.secret_key + "'";
    connection.query($query1, function (err, rows, fields) {
      if (err) {
        result = {
          'status': 'failed',
          'data': 'An error ocurred while performing the query.'
        };
        res.status(500).json(result);
        return;
      }

      if (rows[0]) {
        user_id = rows[0]['user_id'];
        user_type = rows[0]['user_type'];
        name = rows[0]['user_fname'] + " " + rows[0]['user_lname'];
        email = rows[0]['user_email'];
        mobile = rows[0]['user_mobile'];

        user_country = rows[0]['country_name'];
        user_city = rows[0]['user_city'];

        if (user_type != 1) {

          result = {
            'status': 'failed',
            'data': 'You can not post project with developer account!!'
          };
          res.status(200).json(result);

        } else {
          project_alias = seoUrl(project_title);
          $query2 = "INSERT INTO projects(client_id,project_title,project_alias,project_description,project_type,project_state,client_ip) VALUES('" + user_id + "','" + project_title + "','" + project_alias + "','" + project_description + "','0','3','" + ip + "')";

          connection.query($query2, function (err, user_data, fields) {
            if (err) {
              result = {
                'status': 'failed',
                'data': 'An error ocurred while performing the query.'
              };
              res.status(500).json(result);
              return;
            }

            if (user_data.affectedRows > 0) {

              var project_id = user_data.insertId;
              for (var j = 0; j < category.length; j++) {
                $query3 = "INSERT INTO project_categories(project_id,category_id) VALUES('" + project_id + "','" + category[j]['category_id'] + "')";
                connection.query($query3, function (err2, rows2, fields) {
                  if (err2) {
                    result = {
                      'status': 'failed',
                      'data': 'An error ocurred while performing the query.'
                    };
                    res.status(500).json(result);
                    return;
                  }
                });

              }
              ////////////////////////CODE FOR SLACK/////////////////
              var options = {
                "method": "POST",
                "hostname": "sokt.io",
                "port": null,
                "path": "/pWvwy8CSAouSRhT1VMuG/eb-project?project_title=" + urlencode(project_title) + "&fullname=" + urlencode(name) + "&email=" + urlencode(email) + "&mobile=" + urlencode(mobile) + "&detail=" + urlencode(project_description),
                "headers": {
                  "authkey": "ujA3BSHsSh66Qjd2tjaM",
                  "content-type": "application/json"
                }
              };
              var req = http.request(options, function (res) {
                var chunks = [];

                res.on("data", function (chunk) {
                  chunks.push(chunk);
                });

                res.on("end", function () {
                  var body = Buffer.concat(chunks);
                  // console.log(body.toString());
                });
              });

              // req.write(JSON.stringify({ hello: 'world' }));
              req.end();
              ////////////////////////CODE END FOR SLACK/////////////////



              /////////////////////////SEND MAIL TO INTERCOM//////////////////       
              var request = sg.emptyRequest({
                method: 'POST',
                path: '/v3/mail/send',
                body: {
                  personalizations: [{
                    to: [{
                      email: 'kofgg67r@incoming.intercom-mail.com'
                    }],
                    subject: project_title

                  }],
                  from: {
                    email: email
                  },
                  content: [{
                    type: 'text/html',
                    value: project_description
                  }]

                }
              });

              // With promise
              sg.API(request)
                .then(function (response) {
                  // console.log(response.statusCode);
                })
                .catch(function (error) {

                  // console.log(error.response.statusCode);
                });
              //send grid api end  

              /////////////////////////END - SEND MAIL TO INTERCOM//////////////////  

              ///////////////////////// SEND DATA TO INTERCOM /////////////////////////
              var time = new Date().getTime();
              //var time = new Date();

              Iclient.leads.create({
                email: email,
                name: name,
                phone: mobile,
                custom_attributes: {
                  "country_name": user_country,
                  "city_name": user_city
                }

              }, function (r) {



              });
              ///////////////////////// END SEND DATA TO INTERCOM /////////////////////




              result = {
                'status': 'success',
                'isThisNew': '0',
                'data': 'Your project posted successfully.'
              };



              res.status(200).json(result);

            } else {
              result = {
                'status': 'failed'
              };
              res.status(200).json(result);
            }

          });
        }

      } else {
        result = {
          'status': 'failed',
          'data': 'Invalid Access!!'
        };
        res.status(200).json(result);
      }

    });

  } else {
    // user not logged in
    var full_name = req.body.name;
    var user_country = req.body.country;

    var nameList = full_name.split(' ');

    var email = req.body.email;
    var mobile_number = req.body.mobile;
    var time = new Date().getTime();
    var already_login = 0;
    var first_name = '';
    var last_name = '';

    if (nameList.length == 2) {
      first_name = nameList[0].toLowerCase();
      last_name = nameList[1].toLowerCase();
    } else {
      first_name = nameList[0].toLowerCase();
    }

    //check user based on email id
    $query = "SELECT user_email,user_type,user_id,user_password FROM users WHERE user_email ='" + email + "'";
    connection.query($query, function (err, rows, fields) {
      if (err) {
        result = {
          'status': 'failed',
          'data': 'An error ocurred while performing the query.'
        };
        res.status(500).json(result);
        return;
      }

      if (rows.length > 0) {
        //check user type
        if (rows[0]['user_type'] != 1) {
          result = {
            'status': 'failed',
            'data': 'You can not post project with this email, this email registered as developer on this portal !!'
          };
          res.status(200).json(result);
        } else {
          //if user exist so get the user id and insert project

          user_id = rows[0]['user_id'];
          project_alias = seoUrl(project_title);

          if (rows[0]['user_password'] != '') {
            already_login = 1;
          }

          $query2 = "INSERT INTO projects(client_id,project_title,project_alias,project_description,project_type,project_state,client_ip) VALUES('" + user_id + "','" + project_title + "','" + project_alias + "','" + project_description + "','0','3','" + ip + "')";

          connection.query($query2, function (err, user_data, fields) {
            if (err) {
              result = {
                'status': 'failed',
                'data': 'An error ocurred while performing the query.'
              };
              res.status(500).json(result);
              return;
            }

            if (user_data.affectedRows > 0) {

              var project_id = user_data.insertId;
              for (var j = 0; j < category.length; j++) {
                $query2 = "INSERT INTO project_categories(project_id,category_id) VALUES('" + project_id + "','" + category[j]['category_id'] + "')";
                connection.query($query2, function (err2, rows2, fields) {
                  if (err2) {
                    result = {
                      'status': 'failed',
                      'data': 'An error ocurred while performing the query.'
                    };
                    res.status(500).json(result);
                    return;
                  }
                });

              }
              ////////////////////////CODE FOR SLACK/////////////////

              var options = {
                "method": "POST",
                "hostname": "sokt.io",
                "port": null,
                "path": "/pWvwy8CSAouSRhT1VMuG/eb-project?project_title=" + urlencode(project_title) + "&fullname=" + urlencode(full_name) + "&email=" + urlencode(email) + "&mobile=" + urlencode(mobile_number) + "&detail=" + urlencode(project_description),
                "headers": {
                  "authkey": "ujA3BSHsSh66Qjd2tjaM",
                  "content-type": "application/json"
                }
              };
              var req = http.request(options, function (res) {
                var chunks = [];

                res.on("data", function (chunk) {
                  chunks.push(chunk);
                });

                res.on("end", function () {
                  var body = Buffer.concat(chunks);
                  // console.log(body.toString());
                });
              });

              // req.write(JSON.stringify({ hello: 'world' }));
              req.end();
              ////////////////////////CODE END FOR SLACK/////////////////

              /////////////////////////SEND MAIL TO INTERCOM//////////////////       
              var request = sg.emptyRequest({
                method: 'POST',
                path: '/v3/mail/send',
                body: {
                  personalizations: [{
                    to: [{
                      email: 'kofgg67r@incoming.intercom-mail.com'
                    }],
                    subject: project_title

                  }],
                  from: {
                    email: email
                  },
                  content: [{
                    type: 'text/html',
                    value: project_description
                  }]

                }
              });

              // With promise
              sg.API(request)
                .then(function (response) {
                  // console.log(response.statusCode);
                })
                .catch(function (error) {

                  // console.log(error.response.statusCode);
                });
              //send grid api end  

              /////////////////////////END - SEND MAIL TO INTERCOM////////////////// 

              ///////////////////////// SEND DATA TO INTERCOM /////////////////////////
              Iclient.leads.create({
                email: email,
                name: full_name,
                phone: mobile_number,
                custom_attributes: {
                  "country_name": user_country
                }
              }, function (r) {

              });
              ///////////////////////// END SEND DATA TO INTERCOM /////////////////////


              result = {
                'status': 'success',
                'isThisNew': '0',
                'data': 'Your project posted successfully.',
              };



              res.status(200).json(result);

            } else {
              result = {
                'status': 'failed'
              };
              res.status(200).json(result);
            }

          });

        }
      } else {
        // if user not exist, create user first and then insert project.
        // based on email id, name and mobile number.
        // shoot mail only when user not logged in

        time2 = new Date().getTime();
        $query1 = "INSERT INTO users(user_fname,user_lname,user_email,user_mobile,user_type,user_auth_token,email_token,user_country) VALUES('" + first_name + "','" + last_name + "','" + email + "','" + mobile_number + "','1','" + md5(time) + "','" + md5(time2) + "','" + user_country + "')";
        connection.query($query1, function (err2, rows, fields) {
          if (err2) {
            result = {
              'status': 'failed',
              'data': 'An error ocurred while performing the query.'
            };
            res.status(500).json(result);
            return;
          }
          if (rows.affectedRows > 0) {
            user_id = rows.insertId;
            project_alias = seoUrl(project_title);
            $query2 = "INSERT INTO projects(client_id,project_title,project_alias,project_description,project_type,project_state,client_ip) VALUES('" + user_id + "','" + project_title + "','" + project_alias + "','" + project_description + "','0','3','" + ip + "')";

            connection.query($query2, function (err, user_data, fields) {
              if (err) {
                result = {
                  'status': 'failed',
                  'data': 'An error ocurred while performing the query.'
                };
                res.status(500).json(result);
                return;
              }

              if (user_data.affectedRows > 0) {

                var project_id = user_data.insertId;
                for (var j = 0; j < category.length; j++) {
                  $query2 = "INSERT INTO project_categories(project_id,category_id) VALUES('" + project_id + "','" + category[j]['category_id'] + "')";
                  connection.query($query2, function (err2, rows2, fields) {
                    if (err2) {
                      result = {
                        'status': 'failed',
                        'data': 'An error ocurred while performing the query.'
                      };
                      res.status(500).json(result);
                      return;
                    }
                  });

                }
                //update

                ////////////////////////CODE FOR SLACK/////////////////


                var options = {
                  "method": "POST",
                  "hostname": "sokt.io",
                  "port": null,
                  "path": "/pWvwy8CSAouSRhT1VMuG/eb-project?project_title=" + urlencode(project_title) + "&fullname=" + urlencode(full_name) + "&email=" + urlencode(email) + "&mobile=" + urlencode(mobile_number) + "&detail=" + urlencode(project_description),
                  "headers": {
                    "authkey": "ujA3BSHsSh66Qjd2tjaM",
                    "content-type": "application/json"
                  }
                };
                var req = http.request(options, function (res) {
                  var chunks = [];

                  res.on("data", function (chunk) {
                    chunks.push(chunk);
                  });

                  res.on("end", function () {
                    var body = Buffer.concat(chunks);
                    // console.log(body.toString());
                  });
                });

                // req.write(JSON.stringify({ hello: 'world' }));
                req.end();
                ////////////////////////CODE END FOR SLACK/////////////////

                /////////////////////////SEND MAIL TO INTERCOM//////////////////       
                var request = sg.emptyRequest({
                  method: 'POST',
                  path: '/v3/mail/send',
                  body: {
                    personalizations: [{
                      to: [{
                        email: 'kofgg67r@incoming.intercom-mail.com'
                      }],
                      subject: project_title

                    }],
                    from: {
                      email: email
                    },
                    content: [{
                      type: 'text/html',
                      value: project_description
                    }]

                  }
                });

                // With promise
                sg.API(request)
                  .then(function (response) {
                    // console.log(response.statusCode);
                  })
                  .catch(function (error) {

                    // console.log(error.response.statusCode);
                  });
                //send grid api end  

                /////////////////////////END - SEND MAIL TO INTERCOM//////////////////   

                ///////////////////////// SEND DATA TO INTERCOM /////////////////////////
                Iclient.leads.create({
                  email: email,
                  name: full_name,
                  phone: mobile_number,
                  custom_attributes: {
                    "country_name": user_country
                  }

                }, function (r) {
                  ///console.log(r.type);
                });
                ///////////////////////// END SEND DATA TO INTERCOM /////////////////////

                result = {
                  'status': 'success',
                  'isThisNew': '1',
                  'data': 'Projects posted succesfully'
                };
                res.status(200).json(result);

              } else {
                result = {
                  'status': 'failed'
                };
                res.status(200).json(result);
              }

            });

          }


        });

        //shoot mail--------------/////
        //prepare link and text according to user

        var link = '';
        var text = '';

        if (already_login == 1) {
          link = base_url + "login";
          text = "login";
        } else {
          link = base_url + "create-password?email=" + email + "&token=" + md5(time2) + "&verify_type=3";
          text = "verify";
        }
        //send grid api start
        var name = ucfirst(nameList[0].toLowerCase());
        var request = sg.emptyRequest({
          method: 'POST',
          path: '/v3/mail/send',
          body: {
            personalizations: [{
              to: [{
                email: email
              }],
              subject: 'Welcome to EngineerBabu',
              substitutions: {
                "{name}": name,
                "{verify_link}": link
              }
            }],
            from: {
              email: 'no-reply@engineerbabu.com'
            },
            template_id: '045bc594-1320-49e6-b362-4999d44deaf4',
          }
        });

        // With promise
        sg.API(request)
          .then(function (response) {
            // console.log(response.statusCode);
          })
          .catch(function (error) {

            // console.log(error.response.statusCode);
          });
        //send grid api end
      }
    });

  }

});




router.post('/update_userskill', jsonParser, function (req, res) {
  var result = {};
  var user_id;
  var category = [];
  var skills = [];
  var category = req.body.category;
  var skill = req.body.skill;

  $query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "'";
  connection.query($query, function (err, rows, fields) {

    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }

    if (rows[0]) {
      user_id = rows[0]['user_id'];
      $query2 = "DELETE FROM freelancers_categories  WHERE freelancer_id='" + user_id + "'";
      connection.query($query2, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }

      });

      for (var i = 0; i < category.length; i++) {
        $q1 = "INSERT INTO freelancers_categories(freelancer_id,category_id) VALUES('" + user_id + "','" + category[i]['category_id'] + "')";
        connection.query($q1, function (err, rows, fields) {
          if (err) {
            result = {
              'status': 'failed',
              'data': 'An error ocurred while performing the query.'
            };
            res.status(500).json(result);
            return;
          }

        });

      } //end for loop

      $q2 = "update user_state set category_count='" + category.length + "' where user_id='" + user_id + "'";
      connection.query($q2, function (err, q2, fields) {

      });

      $query3 = "DELETE FROM freelancers_skills WHERE freelancer_id='" + user_id + "'";
      connection.query($query3, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }

      });


      for (var j = 0; j < skill.length; j++) {
        $q2 = "INSERT INTO freelancers_skills(freelancer_id,skill_id) VALUES('" + user_id + "','" + skill[j]['skill_id'] + "')";
        connection.query($q2, function (err, rows, fields) {
          if (err) {
            result = {
              'status': 'failed',
              'data': 'An error ocurred while performing the query.'
            };
            res.status(500).json(result);
            return;
          }



        });

      }
      //end for loop


      $q3 = "update user_state set skill_count='" + skill.length + "' where user_id='" + user_id + "'";
      connection.query($q3, function (err, q3, fields) {

      });

      $query4 = "SELECT freelancers_categories.fc_id,categories.* FROM `freelancers_categories` LEFT JOIN categories on freelancers_categories.category_id=categories.category_id WHERE freelancers_categories.freelancer_id='" + user_id + "'";

      connection.query($query4, function (err, categories, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query2.'
          };
          res.status(500).json(result);
          return;
        }
        if (categories[0]) {
          category = categories;

        } else {
          category = "";
        }

        $query5 = "SELECT freelancers_skills.fskill_id,freelancers_skills.skill_id,skills.skill_name FROM `freelancers_skills` LEFT JOIN skills on freelancers_skills.skill_id=skills.skill_id WHERE freelancers_skills.freelancer_id='" + user_id + "'";
        connection.query($query5, function (err, skills_row, fields) {
          if (err) {
            result = {
              'status': 'failed',
              'data': 'An error ocurred while performing the query3.'
            };
            res.status(500).json(result);
            return;
          }

          if (skills_row[0]) {
            skills = skills_row;

          } else {
            skills = "";
          }

        });

      });


      result = {
        'status': 'success'
      };
      res.status(200).json(result);


    } else {
      result = {
        'status': 'failed',
        'data': 'user not valid'
      };
      res.status(200).json(result);
    }



  });

});

router.post('/profile', jsonParser, function (req, res) {
  var result = {};
  var skills = [];
  var sk_arrr = [];

  var category = [];
  var portfolio = [];
  var team = [];
  var review = [];
  var rating = [];
  var profile_count = 0;
  var user_avg_rating = 0;
  var ip = req.connection.remoteAddress;
  var time = new Date().getTime();

  var skill_names = "";

  $query = "SELECT * FROM users WHERE user_username='" + req.body.username + "' AND user_reg_status!='1'  LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {

      var user_id = rows[0]['user_id'];
      $skill_query = "SELECT freelancers_skills.*,skills.skill_name FROM freelancers_skills LEFT JOIN skills ON freelancers_skills.skill_id=skills.skill_id WHERE freelancers_skills.freelancer_id='" + user_id + "' ORDER BY skills.skill_id DESC";
      connection.query($skill_query, function (err, sk_rows, fields) {
        if (sk_rows[0]) {
          skills = sk_rows;

          skills.forEach(function (entry) {
            sk_arrr.push(entry.skill_name);
          });
          skill_names = sk_arrr.toString(',  ');
        }
        $query2 = "SELECT freelancers_categories.fc_id,categories.* FROM `freelancers_categories` LEFT JOIN categories on freelancers_categories.category_id=categories.category_id WHERE freelancers_categories.freelancer_id='" + user_id + "'";
        connection.query($query2, function (err, categories, fields) {
          if (categories[0]) {
            category = categories;
          }

          $query3 = "SELECT freelancers_portfolio.*,CONCAT(LEFT(freelancers_portfolio.fp_desc,250),'...') as fp_desc2,categories.category_name from freelancers_portfolio LEFT JOIN categories ON freelancers_portfolio.category_id=categories.category_id WHERE freelancer_id='" + user_id + "' ";
          connection.query($query3, function (err, portfolios, fields) {
            if (err) {
              result = {
                'status': 'failed',
                'data': 'An error ocurred while performing the query2.'
              };
              res.status(500).json(result);
              return;
            }
            if (portfolios[0]) {
              portfolio = portfolios;

            }

            $query4 = "SELECT * from freelancers_reviews WHERE freelancer_id='" + user_id + "' and report_status=0 and email_verify_status=1";
            connection.query($query4, function (err, reviews, fields) {
              if (reviews[0]) {
                review = reviews;

              }

              $query5 = "SELECT view_count_f,(SELECT avg(review_rating) FROM freelancers_reviews WHERE freelancer_id='" + user_id + "') as user_avg_rating from user_profile_view WHERE user_id='" + user_id + "'";
              connection.query($query5, function (err, profile_counts, fields) {
                if (profile_counts[0]) {
                  profile_count = profile_counts[0].view_count_f;
                  user_avg_rating = Math.round(profile_counts[0].user_avg_rating);
                }

                $query6 = "select * from team where user_id='" + user_id + "'";
                connection.query($query6, function (err, team_row, fields) {
                  if (team_row[0]) {
                    team = team_row;
                  }

                  $query7 = "select SUBSTRING(projects.project_description,1,150) as project_description, rating.*,users.user_fname, SUBSTRING_INDEX(projects.project_title,' ',2) as project_title,projects.project_budget from rating LEFT JOIN users ON users.user_id = rating.client_id LEFT JOIN projects ON projects.project_id = rating.project_id where rating.user_id='" + user_id + "'";
                  connection.query($query7, function (err, rating_row, fields) {
                    if (rating_row[0]) {
                      rating = rating_row;
                    }

                    if (req.body.username != '') {
                      if (req.body.localstorage_username != req.body.username) {

                        $query8 = "select * from user_profile_view where user_id='" + user_id + "'";

                        var view_id;
                        var view_count;
                        var view_count_f;
                        var profile_time;
                        var interval;
                        connection.query($query8, function (err, profile_views, fields) {
                          if (profile_views[0]) {
                            view_id = profile_views[0].view_id;
                            view_count = profile_views[0].view_count + 1;
                            view_count_f = profile_views[0].view_count_f + 1;
                            profile_time = new Date(profile_views[0].last_view).getTime();
                            interval = (time - profile_time) / 3600;
                            if ((ip === profile_views[0].view_ip) && interval < 18000) {} else {
                              $query9 = "UPDATE user_profile_view SET view_count='" + view_count + "',view_count_f='" + view_count_f + "',view_ip='" + ip + "' WHERE view_id='" + view_id + "'";
                              connection.query($query9, function (err, update_count, fields) {});
                              $query11 = "UPDATE user_state SET profile_view_count = profile_view_count+1 where user_id='" + user_id + "'";
                              connection.query($query11, function (err, update_count, fields) {});
                            }
                          } else {
                            $query10 = "INSERT INTO user_profile_view(user_id,view_ip,view_count) VALUES('" + user_id + "','" + ip + "',1)";
                            connection.query($query10, function (err, profile_views, fields) {});
                          }

                        });

                      }

                    }

                    var users = {
                      'hourly_rate': rows[0]['hourly_rate'],
                      'image_handler': rows[0]['image_handler'],
                      'is_online': rows[0]['is_online'],
                      'user_about': rows[0]['user_about'],
                      'user_city': rows[0]['user_city'],
                      'user_company_name': rows[0]['user_company_name'],
                      'user_country': rows[0]['user_country'],
                      'user_designation': rows[0]['user_designation'],
                      'user_fb_url': rows[0]['user_fb_url'],
                      'user_fname': rows[0]['user_fname'],
                      'user_lname': rows[0]['user_lname'],
                      'user_image': rows[0]['user_image'],
                      'user_linkdin_url': rows[0]['user_linkdin_url'],
                      'user_reg_date': rows[0]['user_reg_date'],
                      'user_twitter_url': rows[0]['user_twitter_url'],
                      'user_type': rows[0]['user_type'],
                      'user_url': rows[0]['user_url'],
                      'user_username': rows[0]['user_username']
                      // '': rows[0][''],
                      // '': rows[0][''],
                      // '': rows[0][''],
                    };

                    result = {
                      'status': 'success',
                      'skill_names': skill_names,
                      'data': users,
                      'skills': skills,
                      'category': category,
                      'review': review,
                      'portfolio': portfolio,
                      'profile_count': profile_count,
                      'user_avg_rating': user_avg_rating,
                      'team': team,
                      'rating': rating
                    };
                    res.status(200).json(result);
                  });
                });
              });
            });

          });
        });
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'user not available!!'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});

router.post('/projects', jsonParser, function (req, res) {
  var result = {};
  sql_str = "";
  sql_join = "";
  sql_col = "";
  cates = [];
  var is_apply = "";

  if (typeof req.headers.secret_key != "undefined" || req.headers.secret_key != "" || req.headers.secret_key != null) {

    if (req.body.category_id && req.body.category_id != '') {
      sql_str = " and project_categories.category_id=" + req.body.category_id;
      sql_join = " LEFT JOIN project_categories on p.project_id = project_categories.project_id";
      sql_col = " ,project_categories.category_id";
    }

    $check_query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "'";
    connection.query($check_query, function (err, user_row, fields) {
      if (user_row[0]) {
        var user_id = user_row[0]['user_id'];
        is_apply = ",(SELECT fp_id FROM freelancers_projects fp WHERE p.project_id=fp.project_id AND fp.freelancer_id='" + user_id + "') as apply_id";
      }

      $query = "SELECT p.*,SUBSTRING(p.project_description,1,150) AS project_desc, (select GROUP_CONCAT(categories.category_name SEPARATOR ', ') from project_categories LEFT JOIN categories ON project_categories.category_id = categories.category_id WHERE project_categories.project_id = p.project_id GROUP BY project_categories.project_id) as expertise" + sql_col + "  " + is_apply + " FROM projects p " + sql_join + " WHERE p.project_state='0' " + sql_str + " ORDER BY p.project_id DESC LIMIT 10";
      connection.query($query, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows[0]) {
          result = {
            'status': 'success',
            'data': rows
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'projects not available'
          };
          res.status(200).json(result);
        }
      });
    });
  }

});

router.post('/projects2', jsonParser, function (req, res) {
  var result = {};
  sql_str = "";
  sql_join = "";
  sql_col = "";


  if (req.body.category_id && req.body.category_id != '') {
    sql_str = " and project_categories.category_id=" + req.body.category_id;
    sql_join = " LEFT JOIN project_categories on p.project_id = project_categories.project_id";
    sql_col = " ,project_categories.category_id";
  }
  $query = "SELECT p.*,SUBSTRING(p.project_description,1,300) AS project_desc, (select GROUP_CONCAT(categories.category_name SEPARATOR ', ') from project_categories LEFT JOIN categories ON project_categories.category_id = categories.category_id WHERE project_categories.project_id = p.project_id GROUP BY project_categories.project_id) as expertise" + sql_col + " FROM projects p " + sql_join + " WHERE p.project_state='0' " + sql_str + " ORDER BY p.project_id DESC LIMIT 20";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.',
        'q': $query

      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'projects not available'
      };
      res.status(200).json(result);
    }
  });



});

router.post('/load_more', jsonParser, function (req, res) {
  var result = {};
  sql_str = "";
  sql_join = "";
  sql_col = "";
  var is_apply = "";

  if (typeof req.headers.secret_key != "undefined" || req.headers.secret_key != "" || req.headers.secret_key != null) {
    if (req.body.category_id) {
      sql_str = " and project_categories.category_id=" + req.body.category_id;
      sql_join = " LEFT JOIN project_categories on p.project_id = project_categories.project_id";
      sql_col = " ,project_categories.category_id";
    }

    $check_query = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "'";
    connection.query($check_query, function (err, user_row, fields) {
      if (user_row[0]['user_id']) {
        var user_id = user_row[0]['user_id'];
        is_apply = ",(SELECT fp_id FROM freelancers_projects fp WHERE p.project_id=fp.project_id AND fp.freelancer_id='" + user_id + "') as apply_id";
      }

      $query = "SELECT p.*,SUBSTRING(p.project_description,1,150) AS project_desc, (select GROUP_CONCAT(categories.category_name SEPARATOR ', ') from project_categories LEFT JOIN categories ON project_categories.category_id = categories.category_id WHERE project_categories.project_id = p.project_id GROUP BY project_categories.project_id) as expertise" + sql_col + "  " + is_apply + "  FROM projects p " + sql_join + " WHERE p.project_state='0' " + sql_str + " ORDER BY p.project_id DESC LIMIT " + req.body.offset + ", 10";
      connection.query($query, function (err, rows, fields) {
        if (err) {
          result = {
            'status': 'failed',
            'data': 'An error ocurred while performing the query.'
          };
          res.status(500).json(result);
          return;
        }
        if (rows[0]) {
          result = {
            'status': 'success',
            'data': rows,
            'query': $query
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': 'projects not available'
          };
          res.status(200).json(result);
        }
      });
    });
  }

});

router.post('/load_more2', jsonParser, function (req, res) {
  var result = {};
  sql_str = "";
  sql_join = "";
  sql_col = "";


  if (req.body.category_id) {
    sql_str = " and project_categories.category_id=" + req.body.category_id;
    sql_join = " LEFT JOIN project_categories on p.project_id = project_categories.project_id";
    sql_col = " ,project_categories.category_id";
  }



  $query = "SELECT p.*,SUBSTRING(p.project_description,1,300) AS project_desc, (select GROUP_CONCAT(categories.category_name SEPARATOR ', ') from project_categories LEFT JOIN categories ON project_categories.category_id = categories.category_id WHERE project_categories.project_id = p.project_id GROUP BY project_categories.project_id) as expertise" + sql_col + " FROM projects p " + sql_join + " WHERE p.project_state='0' " + sql_str + " ORDER BY p.project_id DESC LIMIT " + req.body.offset + ", 10";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows,
        'query': $query
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'projects not available'
      };
      res.status(200).json(result);
    }
  });

});


router.get('/client_review', jsonParser, function (req, res) {
  var result = {};
  $query = "SELECT * FROM client_review ";

  connection.query($query, function (err, rows, fields) {
    if (err) {

      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      //console.log("An error ocurred performing the query.");
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'reviews not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});



router.post('/userAppliedProjects', jsonParser, function (req, res) {
  var result = {};
  var applied_project = [];
  $query1 = "SELECT user_id FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query1, function (err, user_rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (user_rows[0]) {
      var user_id = user_rows[0]['user_id'];
      $query = "select *,SUBSTRING(project_description,1,150) AS project_desc , (select GROUP_CONCAT(categories.category_name SEPARATOR ',') from project_categories LEFT JOIN categories ON project_categories.category_id = categories.category_id WHERE project_categories.project_id = projects.project_id GROUP BY project_categories.project_id) as expertise from freelancers_projects Left JOIN projects ON freelancers_projects.project_id=projects.project_id where freelancers_projects.freelancer_id='" + user_id + "' ORDER BY freelancers_projects.fp_id DESC LIMIT 10";

      connection.query($query, function (err, rows, fields) {

        if (rows) {
          applied_project = rows;
          result = {
            'status': 'success',
            'data': applied_project
          };
          res.status(200).json(result);
        } else {
          result = {
            'status': 'failed',
            'data': applied_project
          };
          res.status(200).json(result);
        }
      });

    } else {
      result = {
        'status': 'failed',
        'data': 'login Please'
      };
      res.status(500).json(result);
    }
  });

});


router.post('/getSubscriptionDetail', jsonParser, function (req, res) {
  var result = {};
  var transections = [];
  $query = "SELECT * FROM users WHERE user_auth_token='" + req.headers.secret_key + "' LIMIT 1";
  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var user_id = rows[0]['user_id'];
      $query1 = "SELECT * FROM subscription_log WHERE user_id='" + user_id + "' ORDER by sub_log_id";
      connection.query($query1, function (err, trans_rows, fields) {

        if (trans_rows[0]) {
          transections = trans_rows;
        }

        result = {
          'status': 'success',
          'data': transections,
          'user_data': rows[0]
        };
        res.status(200).json(result);
      });
    } else {
      result = {
        'status': 'failed',
        'data': 'user not found'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });
});

router.post('/applyBid', jsonParser, function (req, res) {
  var result = {};
  var suggestion_letter = req.body.suggestion_letter.replace(/'/g, "");
  var cover_letter = req.body.cover_letter.replace(/'/g, "");

  $query = "SELECT user_id,user_fname,user_lname,user_type,CONCAT(LEFT(users.user_company_name,17),'...') as user_company_name2,image_handler FROM users WHERE user_auth_token='" + req.headers.secret_key + "' AND user_subscription_status='1' LIMIT 1";

  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      var user_id = rows[0]['user_id'];
      var name = "";
      var image_icon = "assets/img/eb_logo_38x38.jpg";

      if (rows[0]['image_handler'] != '') {
        image_icon = "https://cdn.filestackcontent.com/resize=width:38,height:38/" + rows[0]['image_handler'];
      }

      if (rows[0]['user_type'] == "2") {
        name = rows[0]['user_fname'] + " " + rows[0]['user_lname'];
      } else {
        name = rows[0]['user_company_name2'];
      }

      $query1 = "SELECT projects.project_id,projects.client_id,projects.project_alias,projects.project_title,CONCAT(LEFT(projects.project_title,40),'...') as project_title2,users.user_fname,users.user_email FROM projects LEFT JOIN users ON projects.client_id = users.user_id WHERE project_id='" + req.body.project_id + "' AND project_state='0' LIMIT 1";
      connection.query($query1, function (err, pro_rows, fields) {
        if (pro_rows[0]) {


          //$query2 = "SELECT count(*) as bid_count FROM freelancers_projects WHERE MONTH(fp_bid_date) = MONTH(CURRENT_DATE()) AND freelancer_id='" + user_id + "'";
          $query2 = "SELECT remaining_bid from user_state where user_id='" + user_id + "'";
          connection.query($query2, function (err, pro_bid_rows, fields) {


            if (pro_bid_rows[0]['remaining_bid'] > 0) {

              $query4 = "SELECT fp_id FROM freelancers_projects WHERE freelancer_id='" + user_id + "' AND project_id='" + req.body.project_id + "'";
              connection.query($query4, function (err, check_rows, fields) {
                if (check_rows[0]) {
                  result = {
                    'status': 'failed',
                    'data': 'already applied'
                  };
                  res.status(200).json(result);
                } else {
                  var link = [];
                  if (req.body.reffence_link.reffence_link1) {
                    link.push(req.body.reffence_link.reffence_link1);
                    if (req.body.reffence_link.reffence_link2) {
                      link.push(req.body.reffence_link.reffence_link2);

                      if (req.body.reffence_link.reffence_link3) {
                        link.push(req.body.reffence_link.reffence_link3);

                        if (req.body.reffence_link.reffence_link4) {
                          link.push(req.body.reffence_link.reffence_link4);
                        }
                      }
                    }
                  }
                  link = JSON.stringify(link);
                  $query3 = "INSERT INTO freelancers_projects(project_id,freelancer_id,fp_bid_amount,fp_bid_timeframe,fp_bid_cover_letter,pro_suggetion,ref_link) VALUES('" + req.body.project_id + "','" + user_id + "','" + req.body.bid_amount + "','" + req.body.timeframe + "','" + cover_letter + "','" + suggestion_letter + "','" + link + "')";
                  connection.query($query3, function (err, ins_rows, fields) {

                    if ((typeof ins_rows != 'undefined') && (ins_rows.insertId)) {

                      $query5 = "SELECT * FROM freelancers_projects WHERE freelancer_id='" + user_id + "' AND project_id='" + req.body.project_id + "'";
                      connection.query($query5, function (err, get_rows, fields) {

                        $q5 = "UPDATE user_state SET remaining_bid=remaining_bid-1 where user_id='" + user_id + "'";
                        connection.query($q5, function (err, q5, fields) {});

                        var client_name = pro_rows[0]['user_fname'];
                        var client_email = pro_rows[0]['user_email'];
                        var project_title = pro_rows[0]['project_title'];

                        var request = sg.emptyRequest({
                          method: 'POST',
                          path: '/v3/mail/send',
                          body: {
                            personalizations: [{
                              to: [{
                                email: client_email
                              }],
                              subject: 'EngineerBabu: New Bid applied on your project!',
                              substitutions: {
                                "{client_name}": client_name,
                                "{project_title}": project_title,
                                "{client_email}": client_email,
                              }
                            }],
                            from: {
                              email: 'no-reply@engineerbabu.com'
                            },
                            template_id: '5ea80902-db0c-4fa4-be17-09a6ea437e26',
                          }
                        });
                        // With promise
                        sg.API(request)
                          .then(function (response) {
                            // console.log(response.statusCode);
                          })
                          .catch(function (error) {

                          });

                        var project_title2 = pro_rows[0]['project_title2'];
                        var client_id = pro_rows[0].client_id;
                        var project_alias = pro_rows[0].project_alias;
                        var project_id = pro_rows[0].project_id;
                        var notifications_data = JSON.stringify({
                          "notify_type": "project",
                          "image_icon": image_icon,
                          "message": "New bid applied on <b>" + project_title2 + "</b> by <b>" + name + "</b>.",
                          "view_btn": 1,
                          "btn_href": project_alias
                        });

                        $q7 = "INSERT INTO notifications(user_id,from_user_id,project_id,data) VALUES ('" + client_id + "','" + user_id + "','" + project_id + "','" + notifications_data + "')";
                        connection.query($q7, function (err, rows7, fields) {});




                        result = {
                          'status': 'success',
                          'data': 'bid inserted succesfully.',
                          'bid_detail': get_rows[0],
                        };
                        res.status(200).json(result);
                      });

                    } else {
                      result = {
                        'status': 'failed',
                        'data': 'Oops something went wrong, bid not applied.',
                        'bid_detail': []
                      };
                      res.status(200).json(result);
                    }


                  });
                }
              });
            } else {
              result = {
                'status': 'failed',
                'data': 'Sorry, you are out of bid.'
              };
              res.status(200).json(result);
            }
          });
        } else {
          result = {
            'status': 'failed',
            'data': 'This project is not open for bid.'
          };
          res.status(200).json(result);
        }
      });
    } else {
      result = {
        'status': 'failed',
        'data': 'Sorry, your subscription is expired.'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////      main functions     ////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function upload_image_on_s3(img_path, image_type) {

  var folder_name;
  if (image_type == "user") {
    folder_name = 'user_image/';
  } else if (image_type == 'portfolio') {
    folder_name = 'portfolio/';
  } else if (image_type == 'team') {
    folder_name = 'team';
  } else if (image_type == 'client') {
    folder_name = 'client';
  } else {
    //put image in root
    folder_name = '/';
  }


  client = new Upload('ebabu', {
    aws: {
      path: 'mypics',
      region: 'us-west-2',
      acl: 'public-read',
      accessKeyId: 'AKIAJUZZ4QHODA6FAG7Q',
      secretAccessKey: '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'
    },

    cleanup: {
      versions: false,
      original: false
    },

    versions: [{
        maxWidth: 300,
        maxHeight: 300,
        format: 'jpg',
        suffix: '_full',
        quality: 80
      },
      {
        maxHeight: 160,
        maxWidth: 160,

        format: 'jpg',
        suffix: '_normal',
        quality: 80
      },
      {
        maxHeight: 56,
        maxWidth: 56,
        format: 'jpg',
        suffix: '_thumb',
        quality: 80
      }

    ]
  });

}

function calculate_profile_percent(user_info) {
  var profile_com = 0;
  var user_id = user_info['user_id'];
  //reg points

  profile_com = user_info['user_reg_status'] * 5;


  //expertise points
  $query1 = "SELECT count('fc_id') as cat_count FROM freelancers_categories where freelancer_id='" + user_id + "'";
  connection.query($query1, function (err, rows, fields) {

    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['cat_count'] > 0) {
      profile_com = profile_com + 5;
    }

  });


  //skills points
  $query2 = "SELECT count('fskill_id') as skills_count FROM freelancers_skills where freelancer_id='" + user_id + "'";
  connection.query($query2, function (err, rows, fields) {

    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['skills_count'] > 0) {
      profile_com = profile_com + 5;
    }

  });




  //portfolio points

  $query3 = "SELECT count('fp_id') as fp_id FROM freelancers_portfolio where freelancer_id='" + user_id + "'";
  connection.query($query3, function (err, rows, fields) {

    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['fp_id'] > 0) {
      profile_com = profile_com + 10;
    }

  });



  //reviews points

  $query4 = "SELECT count('f_review_id') as f_review_id FROM freelancers_reviews where freelancer_id='" + user_id + "'";
  connection.query($query4, function (err, rows, fields) {

    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]['f_review_id'] > 0) {
      profile_com = profile_com + 10;
    }

  });


  //profile image

  if (user_info['user_image']) {
    profile_com = profile_com + 5;
  }

  //email confirmation
  if (user_info['user_email_confirmed'] == 1) {
    profile_com = profile_com + 5;
  }

  //subscription
  profile_com = profile_com + 25;

  return profile_com;

}

function seoUrl(string) {

  //Lower case everything
  string = string.toLowerCase();
  //Make alphanumeric (removes all other characters)
  //string = string.replace(/[^a-z0-9+]+/gi, '');
  //Clean up multiple dashes or whitespaces
  string = string.replace(/[^\w ]+/g, '');
  //Convert whitespaces and underscore to dash
  string = string.replace(/ +/g, '-');
  var x = Math.floor((Math.random() * 10000) + 1);
  string = string + x
  return string;
}

function ucfirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function getRandomInteger() {
  min = Math.ceil(1);
  max = Math.floor(10000);
  return Math.floor(Math.random() * (max - min)) + min;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////       MOBILE APP      ////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


router.get('/get_skills_by_name_app', jsonParser, function (req, res) {
  var result = {};
  var search_key = req.query.search_key;
  var start = '';
  if (search_key != 0 && search_key != '') {
    var start = "WHERE skill_name like '%" + search_key + "%'";
  }

  $query = "SELECT skill_id,skill_name FROM skills " + start + "";

  // result ={'status':'success','data':$query};
  // res.status(200).json(result);


  connection.query($query, function (err, rows, fields) {
    if (err) {
      result = {
        'status': 'failed',
        'data': 'An error ocurred while performing the query.'
      };
      res.status(500).json(result);
      return;
    }
    if (rows[0]) {
      result = {
        'status': 'success',
        'data': rows
      };
      res.status(200).json(result);
    } else {
      result = {
        'status': 'failed',
        'data': 'Skill not available'
      };
      res.status(200).json(result);
    }
    //console.log("Query succesfully executed: ", rows);
  });

});


module.exports = router;
